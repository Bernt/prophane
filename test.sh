#!/usr/bin/env bash

# set bash mode to strict
set -euo pipefail
IFS=$'\n\t'

# Absolute path this script is in.
PROPHANE_DIR=$(dirname $(readlink -f $0))

if [[ $(conda env list | grep -E "^prophane-test\s") ]]; then
    echo 'removing old environment "prophane-test"...'
    conda env remove -y -q --name prophane-test
fi

echo 'setting up testing environment...'
conda env create -q --name prophane-test --file test_environment.yaml

echo "executing tests"
source $(conda info --root)/etc/profile.d/conda.sh
set +u
conda activate prophane-test
set -u
pytest -v --basetemp=./pytest_tmp tests/tests.py
EXT_CD=$?
conda deactivate

exit "$EXT_CD"

