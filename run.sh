#!/usr/bin/env bash

# set bash mode to strict
set -euo pipefail
IFS=$'\n\t'

# Absolute path this script is in.
PROPHANE_DIR=$(dirname $(readlink -f $0))
FIRST_ARG=${1:-}
CONDA_ENV=${CONDA_ENV:-prophane}

display_help() {
    echo "Prophane Pipeline (powered by Snakemake)"
	echo ""
	echo "Usage: $0 CONFIG_FILE [Snakemake options]"
	echo
	echo " Full list of parameters:"
    echo "   --help                 show Snakemake help (or snakemake -h)"
    echo "   --list-dbs             print list of configured databases"
    echo "                          databases are looked up in 'db_base_dir' configured in:"
    echo "                              ${PROPHANE_DIR}/general_config.yaml"
	echo
    echo " Useful Snakemake parameters:"
	echo "   -j, --cores            number of cores"
	echo "   -k, --keep-going       go on with independent jobs if a job fails"
	echo "   -n, --dryrun           do not execute anything"
	echo "   -p, --printshellcmds   print out the shell commands that will be executed"
	echo "   -t, --timestamp  		add a timestamp to all logging output"
    echo
    exit 0
}

# exit if specified CONDA_ENV is not present
if ! [[ $(conda env list | grep -E "^${CONDA_ENV}\s") ]]; then
    echo "Conda environment '$CONDA_ENV' does not exist. Did you execute setup.sh?"
    exit 1
fi

if [ "$FIRST_ARG" == "--help" ]; then
    snakemake --help
    exit 0
elif [ "$FIRST_ARG" == "" -o "$FIRST_ARG" == "-h" ]; then
    display_help
    exit 0
elif [ "$FIRST_ARG" == "--list-dbs" ]; then
    ${PROPHANE_DIR}/print_database_summary.py
    exit 0
fi

# is first argument a file?
if ! [ -f $FIRST_ARG ]; then
    echo "ERROR"
    echo "${FIRST_ARG}: No such file or directory"
    display_help
    exit 1
fi

# prophane execution
echo "$(date +%Y%m%d-%H:%M:%S) - starting"
echo "activating conda env: ${CONDA_ENV}"
source $(conda info --root)/etc/profile.d/conda.sh
set +u; conda activate ${CONDA_ENV}; set -u
snakemake --snakefile ${PROPHANE_DIR}/Snakefile --use-conda --configfile "$@"
EXT_CD=$?
source deactivate
echo "$(date +%Y%m%d-%H:%M:%S) - all done"

exit "$EXT_CD"
