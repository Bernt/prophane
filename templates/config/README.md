# README: JOB CONFIG FILES IN PROPHANE

## 1. INTRODUCTION
The config files in Prophane are YAML files (https://de.wikipedia.org/wiki/YAML) containing all data to run your analysis.
Ideally, the file is named *config.yaml* and is located in the *path/to/my_job/input* directory.

## 1. GENERAL INFORMATION
```yaml
#general
general:
  job_name: "demonstration job"
  job_comment: "this job is for demonstration use"
```

General job information is stored in the *general* section.

| 2nd level key | value                |
| --------------| -------------------- |
| job_name      | name of your job     |
| job_comment   |  comment on your job |

## 2. INPUT FILES
### 2.1 SCAFFOLD's Protein Report
```yaml
#input
input:
  report: /path/to/jobdir/input/Protein_Report.xls
  report_style: /path/to/prophane3/styles/scaffold_4_8_x.yaml
  fastas:
    - /path/to/jobdir/targetdb.fasta
  taxmaps: []
```

All input files have to be defined in the *input* section.

| 2nd level key | value                                                                 | comment                                |
| --------------| --------------------------------------------------------------------- | -------------------------------------- |
| report        | Scaffold report                                                       | use absolute path                      |
| report_style  | input format style file (YAML)                                        |  use absolute path                     |
| fastas        | list of files (FASTA) containing accession-based sequence information | use absolute paths; use *[]* if empty  |
| taxmaps       |  list of files (TSV) containing accession-based taxonomyinformation   |  use absolute paths; use *[]* if empty |

### 2.2 PD's ProteinXML and PSMS file
```yaml
#input
input:
  pd_xml: /path/to/jobdir/input/protein.xml
  pd_psms: /path/to/jobdir/input/psms.txt
  report_style: /path/to/prophane3/styles/proteome_discoverer_1_4_x.yaml
  fastas:
    - /path/to/jobdir/targetdb.fasta
  taxmaps: []
```

All input files have to be defined in the *input* section.

| 2nd level key | value                                                                  | comment                                |
| ------------- | ---------------------------------------------------------------------- | -------------------------------------- |
| pd_xml        | PD protein xml file                                                    | use absolute path                      |
| pd_psms       | PD psm file                                                            | use absolute path                      |
| report_style  | input format style file (YAML)                                         |  use absolute path                     |
| fastas        | list of files (FASTA) containing accession-based sequence information  | use absolute paths; use *[]* if empty  |
| taxmaps       | list of files (TSV) containing accession-based taxonomyinformation     |  use absolute paths; use *[]* if empty |

### 2.3 MPA's Metaprotein file
```yaml
#input
input:
  mpa_report: /path/to/jobdir/input/metaproteins.txt
  report_style: /path/to/prophane3/styles/mpa_1_8_x.yaml
  fastas:
    - /path/to/jobdir/targetdb.fasta
  taxmaps: []
```

All input files have to be defined in the *input* section.

| 2nd level key | value | comment
| --- | --- | --- |
| mpa_report | MPA metaprotein report | use absolute path |
| report_style |  input format style file (YAML) |  use absolute path |
| fastas  |  list of files (FASTA) containing accession-based sequence information | use absolute paths; use *[]* if empty |
| taxmaps |  list of files (TSV) containing accession-based taxonomyinformation |  use absolute paths; use *[]* if empty |

## 3. OUTPUT DIRECTORY
```yaml
#output directory
output_dir: '/path/to/test_job/'
```

This will be the working directory and the output directory of Prophane for this job.
Please consider, that existing files might be overwritten!


## 4. TASKS
```
#tasks
tasks:
  - ...
  - ...
  - ...
```

Tasks have to be introduced by the 1st level key *tasks:* in the *tasks* followed
by a list of task elements as described in 4.1 to 4.4. There is no limit in number of tasks.
Also same task type might be repeated (e.g. to perform funtional predictions using the same
algorithm but different databases).

| 1st level key | value | comment |
| --- | --- | --- |
| tasks | list of task elements (see 4.1 - 4.4) | use absolute path |


### 4.1 Taxonomic prediction using DIAMOND BLASTP
```yaml
  - prog: 'diamond blastp'
    shortname: tax_from_trembl_20180808_qcover90
    type: taxonomic
    db: /path/to/dbs/trembl/20180808/uniprot_trembl.fasta
    params:
      evalue: 0.01
      query-cover: 0.9
      max-target-seqs: 1
```

DIAMOND BLASTP can be used to transfer taxonomic data from a sequence database such as
NCBI Protein NR or Uniprot based on sequence homology. DIAMOND BLASTP has a lower sensitivity than NCBI BLASTP but is
much faster.

| 2nd level key | value | comment |
| --- | --- | --- |
| prog | program to use | 'diamond blastp' |
| shortname | short descriptive task prediction shown in summary file | avoid spaces, commas, tabulators, and semicolons |
| type | taxonomic | the type of annotation (opposed to functional) |
| db | database FASTA file to search against | use absolute path |
| params | additional DIAMOND BLASTP parameters and values | see for details on DIAMOND BLAST paramters |

### 4.2 Functional prediction using HMMSEARCH or HMMSCAN
```yaml
  - prog: hmmsearch
    shortname: fun_from_TIGRFAMs_15_cut_tc
    db: /path/to/dbs/tigrfams/15/TIGRFAMs_15.0_HMM.LIB
    evalue: 0.01
    cut: cut_tc
    type: functional
```

HMMSEARCH or HMMSCAN can be used to transfer functional data from a HMM library such as
TIGRFAMs or PFAMs based on sequence homology.

| 2nd level key | value | comment |
| --- | --- | --- |
| prog | program to use | 'hmmsearch' or 'hmmscan'|
| shortname | short descriptive task prediction shown in summary file | avoid spaces, commas, tabulators, and semicolons |
| type | functional | the type of annotation (opposed to taxonomic) |
| db | HMM library file to search against | use absolute path |
| params | list of additional HMMSEARCH/HMMSCAN parameters and values | see for details on HMMSEARCH paramters |

### 4.3 Functional prediction using EMAPPER
```yaml
  - prog: emapper
    shortname: fun_from_eggNog_4.5.1
    type: functional
    db: /path/to/dbs/eggnog/4_5_1/eggnog_4_5_1
    params:
      m: diamond
```

EMAPPER can be used to transfer functional data from eggNog based on sequence homology.

| 2nd level key | value | comment |
| --- | --- | --- |
| prog | program to use | emapper |
| shortname | short descriptive task prediction shown in summary file | avoid spaces, commas, tabulators, and semicolons |
| | type | functional | the type of annotation (opposed to taxonomic) |
| db | eggnog database directory  to search against | use absolute path |
| params | additional HMMSEARCH parameters and values | see for details on HMMSEARCH paramters |

## 5. SAMPLES & SAMPLEGROUPS
```yaml
#samples
sample_groups:
    control:
      - "controls::R1"
      - "controls::R2"
      - "controls::R3"
    treated:
      - "treated::R1"
      - "treated::R2"
      - "treated::R3"
```

Samples and sample groups can be defined in the *sample_groups* section.
Sample groups are used to calculate mean NSAF based on all assigned samples.
There is now limit in number of sample groups, however, at least one has to be defined.
All samples in your report have to be assigned to exactly one sample group.

| 2nd level key | value | comment |
| --- | --- | --- |
| user_defined | list of sample names assigend to the respective group |

Sample names can be found in your MS result reports.

| report type | sample column | comment |
| --- | --- | --- |
| SCAFFOLD's Protein Report | *Biological sample category*, *Biological sample name* | values of both columns have to be fused by *::* |
| PD's Protein Report | samples are automatically retrieved | |
| MPA's Metaprotein Report | *sample* | column has to be manually inserted to the given report |

## 6. Recognition of DECOY sequences
```yaml
#decoy
decoy_regex: DECOY$
```

An accession pattern of decoy sequences can be defined in the *decoy* section.
Matching accessions are excluded from the analysis.

| 1st level key | value | comment |
| --- | --- | --- |
| decoy_regex | regex exclusively matching to  decoy accessions | use python-type regex |

## 7. NSAF calculation method

This parameter is optional. If not set, it will default to *max_nsaf*.

```yaml
#quant_method
quant_method: max_nsaf
```

The method of NSAF calculation can be defined in the *quant_method* section.

| 1st level key | value | comment |
| --- | --- | --- |
| quant_method | method of NSAF calculation | *max_nsaf*, *min_nsaf*, *mean_nsaf*, *raw* |

Following NSAF calculation methods can be selected differing in the used sequence length.

| quant_method | sequence length used |
| --- | --- |
| max_nsaf | longest sequence in the respective protein group |
| min_nsaf | shortest sequence in the respective protein group |
| mean_nsaf | mean length of all sequences in the respective protein group |
| raw | unprocessed spectral counts |


## 8. LCA labeling
```yaml
#LCA labeling
dictionary:
  unclassified: unclassified
  heterogenous: various
```

LCA labeling can be adapted in the *dictionary* section.

| 2nd level key | value |
| --- | --- |
| unclassified | label of unclassified protein groups |
| various | label of ambigiously classified protein groups |


## 9. Additional Information
```yaml
#additional
seqdb:
  - /path/to/dbs/skip/skip.fa
taxmaps:
  - /path/to/dbs/skip/skip.map
taxlevel:
  - superkingdom
  - class
  - order
  - family
  - genus
  - species
```

Information in the *Additional Information* is partly required by Prophane at the moment. This will change in future Prophane versions.

Mandatory:
- seqdb
- taxmaps

Optional:
- taxlevel

| 1st level key | value | comment |
| --- | --- | --- |
| seqdb | empty file | use absolute path |
| taxmaps | empty file | use absolute path |
| taxlevel | list of taxonomic levels to consider | if changed, db-specific taxmaps have to be rebuilt! |
