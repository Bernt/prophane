rule create_pg_fasta:
  input:
    fasta = 'seqs/all.faa',
    pg_yaml = 'pgs/protein_groups.yaml',
  output:
    expand('seqs/pg.{n}.faa', n=find_groups_to_align('pgs/protein_groups.yaml'))
  message:
    "creating FASTA files for protein groups ..."
  params:
    path_template=lambda x: os.path.join(os.getcwd(), 'seqs/pg.{n}.faa')
  version:
    "0.2"
  run:
    data = load_yaml(input.pg_yaml)
    sep = data['style']['protein_sep']
    pgs = data['protein_groups']
    group_ids = find_groups_to_align('pgs/protein_groups.yaml')
    accs = []
    for group_id in group_ids:
        accs.append(set(pgs[group_id]['proteins'].split(sep)))

    started_groups = set()
    l = range(len(accs))
    with open(input.fasta, "r") as inhandle:
        for line in inhandle:
            if len(line.strip()) == 0:
                continue
            acc = line
            seq = inhandle.readline()
            for i in l:
                if acc[1:-1] in accs[i]:
                    group_id = group_ids[i]
                    if not group_id in started_groups:
                        started_groups.add(group_id)
                        mode = "w"
                    else:
                        mode = "a"
                    with open(params.path_template.format(n=group_id), mode) as outhandle:
                        outhandle.write(acc+seq)
