rule make_tigrfam_roles:
  input:
    "{tigrfamsdb}_ROLE_NAMES",
    "{tigrfamsdb}_ROLE_LINK",
    "{tigrfamsdb}_INFO.tar.gz"
  output:
    "{tigrfamsdb}_HMM.map"
  wildcard_constraints:
    fname = ".*/TIGRFAMs[^/]+"
  message:
    "generating map {output[0]}"
  version:
    "0.1"
  run:
    role_names = defaultdict(dict)
    with open(input[0], "r") as handle:
        for line in handle:
            fields = line.strip().split("\t")
            role_names[fields[1]][fields[2].rstrip(":")] = fields[3]


    role_links = {}
    with open(input[1], "r") as handle:
        for line in handle:
            fields = line.strip().split("\t")
            role_links[fields[0]] = fields[1]

    with open(output[0], "w") as handle:
        handle.write("#tigrfam\tmainrole\tsubrole\tdescr\ttigrfam\n")
        tar = tarfile.open(input[2], "r:gz")
        tar_members = tar.getnames()
        for member in tar_members:
            fam = ""
            descr = ""
            for line in tar.extractfile(member):
                if line.startswith(b"AC "):
                    fam = line[4:].decode().strip()
                elif line.startswith(b"DE "):
                    descr = line[4:].decode().strip()

            if fam not in role_links or role_links[fam] not in role_names:
                mainrole = config['dictionary']['unclassified']
                subrole = config['dictionary']['unclassified']
            else:
                roleid = role_links[fam]
                mainrole = role_names[roleid]['mainrole']
                subrole = role_names[roleid]['sub1role']

            handle.write(fam + "\t" + mainrole + "\t" + subrole + "\t" + descr + "\t" + fam + "\n")
