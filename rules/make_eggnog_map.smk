def input_make_eggnog_map(wildcards):
    dbinfo = get_db_info(wildcards.db + ".yaml")
    annotation_file = dbinfo['og_annotations']
    funcat_file = dbinfo['fun_cats']
    return [annotation_file, funcat_file]

rule make_eggnog_map:
  input:
    input_make_eggnog_map
  output:
    "{db}.map"
  wildcard_constraints:
    db = ".*/eggnog_[^/]+"
  message:
      "generating database map ..."
  run:
    #read eggnog funcats
    funcats = {}
    with open(input[1], "r") as handle:
        for line in handle:
            line = line.strip()
            if len(line) == 0:
                continue
            elif line[0] != "[":
                mainrole = line.lower()
            else:
                key = line[1]
                subrole = line[4:]
                if key in funcats:
                    sys.exit("error: multiple funcyional roles for key ''" + key + "' in " + input[1])
                funcats[key] = (mainrole, subrole)

    #read eggnog og annotations
    na = set()
    with gzip.open(input[0], 'rb') as gzhandle, open(output[0], 'w') as outhandle:
      inhandle = io.BufferedReader(gzhandle)
      outhandle.write("#og.taxid.prot\tmain_role\tsub_role\tog\tdescr\tprot\n")
      for line in inhandle:
        line = line.decode().strip()
        if len(line) == 0:
            continue
        fields = line.split("\t")
        og = fields[0]
        prots = fields[-1].split(",")
        descr = fields [3]
        fun_key = fields[4].split("'")[1]
        mainrole = funcats[fun_key][0]
        subrole = funcats[fun_key][1]
        for prot in prots:
            if prot not in na:
                na.add(prot)
                outhandle.write("NA" + "." + prot + "\t" + mainrole + "\t" + subrole + "\t" + og + "\t" + descr + "\t" + prot + "\n")
            outhandle.write(og + "." + prot + "\t" + mainrole + "\t" + subrole + "\t" + og + "\t" + descr + "\t" + prot + "\n")
