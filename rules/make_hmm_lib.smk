rule make_hmm_lib:
  input:
    "{hmmlib}"
  output:
    "{hmmlib}.h3f",
    "{hmmlib}.h3i",
    "{hmmlib}.h3m",
    "{hmmlib}.h3p"
  message:
    "preparing hmm db for first use: " + os.path.basename("{input[0]}")
  log:
    "{hmmlib}.log"
  version:
    "0.11"
  conda:
    "../envs/prophane.yaml"
  threads: 4
  shell:
    '''
    hmmpress {input[0]} >> {log} 2>&1
    '''
