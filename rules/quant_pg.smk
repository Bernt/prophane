def calc_nsafs(scounts, protlens):
    safs = [x[0] / x[1] for x in zip(scounts, protlens)]
    total = sum(safs)
    return [x/total for x in safs]

rule quant_pg:
  input:
    fasta = 'seqs/all.faa',
    pg_yaml = 'pgs/protein_groups.yaml'
  output:
    'tasks/quant.tsv'
  message:
    "creating FASTA files for protein groups ..."
  version:
    "0.1"
  params:
    quant_methods = {"min_nsaf", "max_nsaf", "mean_nsaf", "raw"}
  run:
    #read pgs
    pgs = load_yaml(input.pg_yaml)
    sep = pgs['style']['protein_sep']
    quant_method = config['quant_method']
    if quant_method not in params.quant_methods:
        trace("config error:\nunkown quantification method (quant_method=" + quant_method + ")", True)

    #store raw quant data
    i = -1
    samples = pgs['samples']
    pg_members = []
    pg_raw_quant = []
    all_members = set()
    for pg in pgs['protein_groups']:
        i += 1
        pg_members.append(pg['proteins'].split(sep))
        all_members.update(pg_members[-1])
        pg_raw_quant.append(pg['quant'])

    #read seqs
    seqlens = {}
    for line in iter_file(input.fasta):
        if line[0] == ">":
            acc = line[1:]
        elif acc in all_members:
            l = len(line)
            if acc in seqlens and l != seqlens[acc]:
                trace("quantification error:\n"+ 'ambigious sequences for ' + acc + " in " + input.fasta, True)
            seqlens[acc] = l

    #get member seqlens
    pg_lens = []
    final_quant = []
    for i in range(len(pg_members)):
        pg_lens.append([seqlens[x] for x in pg_members[i]])
        final_quant.append([])

    if quant_method == "min_nsaf":
        processed_pg_lens = [min(x) for x in pg_lens]
    elif quant_method == "max_nsaf":
        processed_pg_lens = [max(x) for x in pg_lens]
    elif quant_method == "mean_nsaf":
        processed_pg_lens = [sum(x)/len(x) for x in pg_lens]

    #calc quant
    if quant_method == "raw":
        final_quant = pg_raw_quant
    else:
        for i in range(len(samples)):
            raw_quants = [x[i] for x in pg_raw_quant]
            i = 0
            for nsaf in calc_nsafs(raw_quants, processed_pg_lens):
                final_quant[i].append(nsaf)
                i += 1

    #get sample indices for mean quant
    if 'sample_groups' in config:
        sg_indices = {}
        groups = sorted(config['sample_groups'].keys())
        for sg, samplelist in config['sample_groups'].items():
            sg_indices[sg] = [samples.index(x) for x in samplelist]

    #calc mean quant and write out
    with open(output[0], "w") as handle:
        handle.write("#pg\tsample\tmin_seqlen\tmax_seqlen\traw_quant\traw_quant_sd\tquant\tquant_sd\n")
        for i in range(len(final_quant)):
            if 'sample_groups' in config:
                for g in groups:
                    g_raw_quant = [pg_raw_quant[i][x] for x in sg_indices[g]]
                    g_quant = [final_quant[i][x] for x in sg_indices[g]]
                    handle.write(str(i) + "\t" + g + " (mean)\t" + "-" + "\t" + "-" + "\t" + str(numpy.mean(g_raw_quant)) + "\t" + str(numpy.std(g_raw_quant)) + "\t" + str(numpy.mean(g_quant)) + "\t" + str(numpy.std(g_quant)) + "\n")
            for j in range(len(samples)):
                handle.write(str(i) + "\t" + samples[j] + "\t" + str(min(pg_lens[i])) + "\t" + str(max(pg_lens[i])) + "\t" + str(pg_raw_quant[i][j]) + "\t-\t" + str(final_quant[i][j]) + "\t-\n")
