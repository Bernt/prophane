def input_make_uniprot_map(wildcards):
    path = os.path.dirname(wildcards.taxmap)
    idmap = os.path.join(path, "idmapping.dat.gz")
    taxmap = os.path.join(path, "uniprot_tax.txt") #download by this link https://www.uniprot.org/taxonomy/?sort=&desc=&compress=no&query=&fil=&force=no&preview=true&format=tab&columns=id
    return [taxmap, idmap]

rule make_idmapping_map:
  input:
    input_make_idmapping_map
  output:
    "{fname}.map",
  wildcard_constraints:
    fname = ".*/idmapping.dat"
  message:
    "generating map {output[0]}"
  run:
    taxa = {}
    with open(input[1], "r") as handle:
        headline = handle.readline()[1:]
        for line in handle:
            taxid = line.split("\t")[0]
            taxa[taxid] = line

    with gzip.open(input[0], 'rb') as gzhandle, open(output[0], 'w') as outhandle, open(output[1], 'w') as outhandle2:
      outhandle.write("#accession\t" + headline)
      inhandle = io.BufferedReader(gzhandle)
      for line in inhandle:
        fields = line.decode().split("\t")
        if fields[1] == "NCBI_TaxID":
            if fields[2].strip() not in taxa:
                outhandle2.write(fields[0] + '\t' + fields[2].strip() + "\n")
            else:
                outhandle.write(fields[0] + '\t' + taxa[fields[2].strip()] + "\n")
