def input_create_summary(wildcards):
    pg_yaml = 'pgs/protein_groups.yaml'
    files = [pg_yaml]
    for i in range(len(config['tasks'])):
        files.append(get_task_lca(i))
    for i in range(len(config['tasks'])):
        files.append(get_task_map(i))
    files.append('tasks/quant.tsv')
    pg_data = load_yaml(pg_yaml)
    pgs = pg_data['protein_groups']
    sep = pg_data['style']['protein_sep']
    return files

rule create_summary:
  input:
    input_create_summary,
    expand('algn/mafft.{n}.txt', n = find_groups_to_align('pgs/protein_groups.yaml'))
  output:
    'summary.txt'
  message:
    "creating summary ..."
  version:
    "0.1"
  run:
    #config
    quant_file = os.path.join(os.getcwd(), 'tasks/quant.tsv')
    pgs = load_yaml(input[0])
    sep = pgs['style']['protein_sep']
    task_count = len(config['tasks'])
    task_range = range(task_count)

    #general data
    headline_data = [["level", "#pg", "members_count", "min_seqlen", "max_seqlen", "min_rel_pairw_ident", "max_rel_pairw_ident", "members_identifier"]]
    group_data = []
    for i in range(len(pgs['protein_groups'])):
        pg = pgs['protein_groups'][i]
        group_data.append([])

        accs = pg['proteins'].split(sep)

        mr = get_mafft_report(i)
        if mr in input:
            with open(mr, "r") as handle:
                min_seqlen, max_seqlen, min_ident, max_ident = handle.readlines()[-1].strip().split("\t")
        else:
                min_seqlen = max_seqlen = min_ident = max_ident = "-"

        group_data[-1].append([str(i), str(len(accs)), min_seqlen, max_seqlen, min_ident, max_ident, ";".join(accs)])

    #task data
    member_data = {}
    for acc in get_accs(input[0]):
        member_data[acc] = []
        for t in task_range:
            member_data[acc].append(set())

    for t in task_range:
        #task data headline
        hl = ["task_" + str(t) + "::" + config['tasks'][t]['shortname'].replace(" ", "_") + "::" + x.replace("_", " ") for x in get_headline(input[t+1]).split("\t")]
        headline_data.append(hl[1:])

        #task group data
        for line in iter_file(input[t+1]):
            fields = line.split("\t")
            group_data[int(fields[0])].append(fields[1:])

        #task member_data
        l = -len(hl)+1
        for line in iter_file(input[t+1+task_count]):
            fields = line.split("\t")
            member_data[fields[0]][t].add("\t".join(fields[l:]))

        empty = "\t".join([config['dictionary']['unclassified']] * (-l))
        for acc in member_data:
            if len(member_data[acc][t]) == 0:
                member_data[acc][t].add(empty)

    #quant data
    hl = get_headline(quant_file).split("\t")[-4:]
    cols = []
    if 'sample_groups' in config:
        for group in sorted(config['sample_groups'].keys()):
            cols.append(group + " (mean)")
            for sample in config['sample_groups'][group]:
                cols.append(sample)
    else:
        for sample in pgs['samples']:
            cols.append(sample)

    for col in cols:
        headline_data.append([x + "::" + col for x in hl])
    quant_data = []
    l = len(cols)
    for i in range(len(pgs['protein_groups'])):
        quant_data.append([])
        for j in range(l):
            quant_data[-1].append([])

    for line in iter_file(quant_file):
        fields = line.split("\t")
        col = cols.index(fields[1])
        quant_data[int(fields[0])][col] = fields[-4:]


    #write out
    with open(output[0], "w") as handle:
        handle.write("\t".join(["\t".join(x) for x in headline_data]) + "\n")
        for i in range(len(group_data)):
            g = ['group'] + ["\t".join(x) for x in group_data[i]]
            q = ["\t".join(x) for x in quant_data[i]]
            handle.write("\t".join(g + q) + "\n")

            for acc in pgs['protein_groups'][i]['proteins'].split(sep):
                data = [sorted(x) for x in member_data[acc]]
                rows = max([len(x) for x in data])
                for r in range(rows):
                    out = ["member", str(i), "-", "-", "-", "-", "-", acc]
                    for t in task_range:
                        if len(data[t]) > r and len(data[t][r]) > 0:
                            out.append(data[t][r])
                        else:
                            out.extend("-" * len(headline_data[t+1]))
                    out.extend("-" * len(q))
                    handle.write("\t".join(out) + "\n")
