rule make_pfam_map:
  input:
    "{pfamsdb}.clan.txt", #source: ftp://ftp.ebi.ac.uk/pub/databases/Pfam/current_release/database_files/clan.txt.gz
    "{pfamsdb}.clans.tsv"
  output:
    "{pfamsdb}.map"
  message:
    "generating map {output[0]}"
  wildcard_constraints:
    pfamsdb = ".*/Pfam[^/]+"
  run:
    clan_infos = {}
    with open(input[0], "r") as handle:
        for line in handle:
            fields = line.strip().split("\t")
            clan_infos[fields[0]] = (fields[1], fields[3])

    with open(input[1], "r") as inhandle, open(output[0], "w") as outhandle:
        outhandle.write("pfam\tclan\tclan_symbol\tclan_descr\tpfam_descr\tpfam\n")
        for line in inhandle:
            fields = line.strip().split("\t")
            pfam = fields[0]
            clan = fields[1].strip()
            if not clan:
                clan = config['dictionary']['unclassified']
                clan_symbol = "-"
                clan_descr = "-"
            elif clan in clan_infos:
                clan_symbol = clan_infos[fields[1]][0]
                clan_descr = clan_infos[fields[1]][1]
            else:
                clan_symbol = "-"
                clan_descr = "-"
            pfam_descr = fields[-1]

            outhandle.write(pfam + "\t" + clan + "\t" + clan_symbol + "\t" + clan_descr + "\t" + pfam_descr + "\t" + pfam + "\n")
