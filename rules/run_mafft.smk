rule run_mafft:
  input:
    'seqs/pg.{n}.faa'
  output:
    'algn/pg.{n}.mafft'
  message:
    "aligning group members ..."
  conda:
    "../envs/prophane.yaml"
  threads: 1
  shell:
    '''
    mafft --auto --quiet --anysymbol --thread {threads} {input[0]} > {output[0]}
    '''
