import os
from utils.db_handling import DbAccessor

def get_query_fasta(wildcards):
    task = config['tasks'][int(wildcards.n)]
    if task['type'] == 'functional':
        query = 'seqs/all.faa'
    elif task['type'] == 'taxonomic':
        query = 'seqs/missing_taxa.faa'
    else:
        raise ValueError("Task type must be 'functional' or 'taxonomic' but is: {}".format(task['type']))
    return query


def get_preprocessed_dbfiles(wildcards):
    task = config['tasks'][int(wildcards.n)]
    prog = task['prog']
    db_yaml = get_db_yaml(task)
    db_files = []
    if prog.startswith("diamond "):
        db_files = [os.path.splitext(db_yaml)[0] + ".dmnd"]
    elif prog.startswith("hmm"):
        db_files = expand(get_prog_db_string(wildcards) + ".{ext}", ext=get_hmm_exts())
    elif prog.startswith("emapper"):
        db_files = [os.path.splitext(db_yaml)[0] + ".map"]
    else:
        trace("config error:\nunsafe command (" + prog + ") in task " + wildcards.n, True)
    return db_files


def get_prog_db_string(wildcards):
    task = config['tasks'][int(wildcards.n)]
    prog = task['prog']
    db_yaml = get_db_yaml(task)
    prog_db_string = None
    if prog.startswith("diamond "):
        prog_db_string = os.path.splitext(db_yaml)[0] + ".dmnd"
    elif prog.startswith("hmm"):
        if '/tigrfams/' in db_yaml:
            prog_db_string = os.path.splitext(db_yaml)[0] + ".LIB"
        elif '/pfams/' in db_yaml:
            prog_db_string = os.path.splitext(db_yaml)[0] + ".hmm"
        else:
            raise ValueError("Unknown database type: \n\t{}".format(db_yaml))
    elif prog.startswith("emapper"):
        prog_db_string = os.path.join(os.path.dirname(db_yaml), "data")
    else:
        trace("config error:\nunsafe command (" + prog + ") in task " + wildcards.n, True)
    return prog_db_string


rule build_run_task_command:
  input:
    query_fasta=get_query_fasta,
    db_yaml=lambda w: get_db_yaml(config['tasks'][int(w.n)]),
    preprocessed_dbfiles=get_preprocessed_dbfiles
  output:
    cmd_file='tasks/task.{n}.result-cmd.txt',
    yaml='tasks/task.{n}.yaml'
  version:
    "0.1"
  log:
    'tasks/task.{n}.log'
  params:
    prog_db_string = lambda w: get_prog_db_string(w)
  threads:
    32
  run:
    """
    Build the command for annotation of provided query file and write it to text file.
    Write out parameters to task specific yaml file.
    """
    task = config['tasks'][int(wildcards.n)]
    query_file = input['query_fasta']
    prog_db_string = params['prog_db_string']
    annot_result = re.sub(r"-cmd\.txt$", "", output.cmd_file)
    log_file = log[0]

    print("working on " + task['shortname'] + " with " + str(threads) + " threads")

    #no params
    if 'params' not in task:
        task['params'] = {}

    #empty query
    if os.path.getsize(query_file) == 0:
        for o in output:
            open(o, "w").close()

    #diamond blastp
    elif task['prog'] == "diamond blastp":
        task['params']['outfmt'] = '6 qseqid sallseqid qlen evalue bitscore length nident'
        task['params']['out'] = annot_result
        task['params']['query'] = query_file
        task['params']['db'] = prog_db_string
        task['params']['quiet'] = ""
        task['params']['threads'] = threads
        task['params']['top'] = 0

        cmd = task['prog'] + " " + " ".join(['--' + x[0] + " " + str(x[1]) for x in task['params'].items()])

    #hmmer
    elif task['prog'] == "hmmscan":
        single_args = {'Z', 'T', 'E'}
        cmd = task['prog'] + " --noali --notextw --cpu " + str(threads) + " --tblout " + \
              annot_result + " -o " + log_file
        cmd += " " + " ".join(['--' + x[0] + " " + str(x[1]) if x[0] not in single_args else '-' + x[0] + " " + str(x[1]) for x in task['params'].items()])
        cmd += " " + prog_db_string + " " + query_file

    elif task['prog'] == "hmmsearch":
        single_args = {'Z', 'T', 'E'}
        cmd = task['prog'] + " --noali --notextw --cpu " + str(threads) + " --tblout " + \
              annot_result + " -o " + log_file
        cmd += " " + " ".join(['--' + x[0] + " " + str(x[1]) if x[0] not in single_args else '-' + x[0] + " " + str(x[1]) for x in task['params'].items()])
        cmd += " " + prog_db_string + " " + query_file

    #egnogg mapper
    elif task['prog'] == "emapper":
        single_args = {'m'}
        task['params']['data_dir'] = prog_db_string
        task['params']['output'] = annot_result
        task['params']['cpu'] = threads

        #cmd = "/home/stephan/bin/eggnog-mapper/emapper.py " + " ".join(['--' + x[0] + " " + str(x[1]) for x in task['params'].items()])
        cmd = "emapper.py "
        cmd += " -i " + query_file + " --override"
        cmd += " " + " ".join(['--' + x[0] + " " + str(x[1]) if x[1] not in single_args else '-' + x[0] + " " + str(x[1]) for x in task['params'].items()])

    #unknown tools
    else:
        trace("error: task " + wildcards.n + ": '" + task['prog'] + "' not allowed.", True)

    # write cmd_file
    with open(output.cmd_file, "w") as handle:
        handle.write(cmd)
        print(cmd)
        handle.write("\n")

    #write yaml
    with open(output.yaml, "w") as handle:
        task = dict(task)
        if params in task:
            task['params'] = dict(task['params'])
        handle.write(yaml.dump(task))


rule run_task:
  input:
    cmd_file='tasks/task.{n}.result-cmd.txt'
  output:
    'tasks/task.{n}.result'
  message:
    "performing task {wildcards.n} ..."
  conda:
    "../envs/prophane.yaml"
  log:
    'tasks/task.{n}.log'
  version:
    "0.1"
  threads:
    32
  params:
    tool=lambda w: config['tasks'][int(w.n)]['prog']
  shell:
    """
    # execute command file
    bash {input.cmd_file}
    # cp eggnog result: output[0] + ".emapper.annotations" -> output[0]
    if [ '{params.tool}' = 'emapper' ]; then
      cp "{output[0]}.emapper.annotations" {output[0]}
    fi
    """
