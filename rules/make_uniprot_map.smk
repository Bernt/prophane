def input_make_uniprot_map(wildcards):
    dbinfo = get_db_info(wildcards.fname + ".yaml")
    db = wildcards.fname + ".gz"
    taxmap = dbinfo['taxmap']
    return [db, taxmap]

rule make_uniprot_map:
  input:
    input_make_uniprot_map
  output:
    "{fname}.map"
  wildcard_constraints:
    fname = ".*/uniprot_(?!tax)[^/]+"
  message:
    "generating taxmap {output[0]}"
  run:
    #get all accs
    dbinfo = get_db_info(wildcards.fname + ".yaml")
    accs = set()
    acc_regexp = re.compile(dbinfo['acc_regexp'])
    l = len(dbinfo['tag'])
    with gzip.open(input[0], "rt") as handle:
        for line in handle:
            if line[0] == ">":
                m = acc_regexp.search(line)
                if m:
                    acc = m.group(0)[l:]
                    accs.add(acc)
    #create map
    with open(input[1], 'r') as inhandle, open(output[0], 'w') as outhandle:
      outhandle.write(inhandle.readline())
      for line in inhandle:
          if line.split("\t")[0] in accs:
              outhandle.write(line)
