rule create_info:
  input:
    'summary.txt',
    all_faa='seqs/all.faa'
  output:
    'job_info.txt'
  message:
    "writing job_info ..."
  version:
    "0.1"
  run:
    # env_yaml = load_yaml(os.path.join(os.path.dirname(workflow.snakefile), "envs/prophane.yaml"))
    # vers = {}
    # for prog in env_yaml['dependencies']:
    #     if isinstance(prog,str):
    #         fields = prog.strip().split("=")
    #         vers[fields[0]] = fields[1]

    # not in input because this file is produced conditionally
    missing_taxa_faa='seqs/missing_taxa.faa'

    if "jobname" in config:
        jobname = config['general']['job_name']
        jobname = config['general']['job_comment']


    out = []
    out.append("PROPHANE JOB INFORMATION")
    out.append('')
    out.append('a. general information')
    out.append('   job name: ' + config['general']['job_name'])
    out.append('   job comment: ' + config['general']['job_comment'])
    out.append('   start time: ' + START_TIME)
    out.append('   end time: ' + get_timestamp())
    out.append('')
    out.append('b. program versions')
    out.append('   prophane: ' + PROPHANE_VERSION)
    # out.append('   blast+: ' + vers['blast'])
    # out.append('   diamond: ' + vers['diamond'])
    # out.append('   hmmer: ' + vers['hmmer'])
    # out.append('   krona: ' + vers['krona'])
    out.append('')
    out.append('c. input')
    if 'report' in config['input']:
        out.append('   protein report (Scaffold): ' + os.path.basename(config['input']['report']))
        out.append('   report style: ' + os.path.basename(config['input']['report_style']))
    elif 'pdxml' in config['input']:
        out.append('   protein xml (Proteome Discoverer): ' + os.path.basename(config['input']['pdxml']))
        out.append('   psms txt (Proteome Discoverer): ' + os.path.basename(config['input']['pdpsms']))
        out.append('   xml/psms style: ' + os.path.basename(config['input']['report_style']))
    if not config['input']['fastas']:
        out.append('   sequence data: None')
    else:
        out.append('   sequence data:')
        for fname in config['input']['fastas']:
            out.append('      - ' + os.path.basename(fname))
    if not config['input']['taxmaps']:
        out.append('   taxonomic data: None')
    else:
        out.append('   taxonomic data:')
        for fname in config['input']['taxmaps']:
            out.append('      - ' + os.path.basename(fname))
    out.append('')
    out.append('d. sample groups')
    if 'sample_groups' not in config:
        out.append('   not defined')
    else:
        for g in sorted(config['sample_groups'].keys()):
            out.append('   ' + g)
            for s in config['sample_groups'][g]:
                out.append('      - ' + s)
    out.append('')
    out.append('e. tasks')
    n = -1
    for task in config['tasks']:
        n += 1
        dbinfo = get_db_info(get_db_yaml(task))
        out.append('   task: ' + str(n))
        out.append('   task comment: ' + task['shortname'])
        out.append('   algorithm: ' + task['prog'])
        if task['type'] == 'taxonomic':
            out.append('   query: sequences without taxonomic classification (' + missing_taxa_faa + ')')
        elif task['type'] == 'functional':
            out.append('   query: all sequences (' + input['all_faa'] + ')')
        else:
            raise ValueError("Type of task {n} is only allowed to be 'functional' or 'taxonomic' but is {val}" \
                             .format(n=n, val=task['type']))
        out.append('   database: ' + dbinfo['name'])
        out.append('   database version: ' + str(dbinfo['version']))
        out.append('   database comment: ' + dbinfo['comment'])
        out.append('   database scope: ' + dbinfo['scope'])
        out.append('   parameter:')
        if 'params' in task:
            for param, val in task['params'].items():
                out.append('      - ' + param + " " + str(val))
        for param, val in task.items():
            if param in ['prog', 'shortname', 'query', 'db', 'params']:
                continue
            out.append('      - ' + param + "  " + str(val))
        out.append('')

    with open(output[0], "w") as handle:
        handle.write("\n".join(out))
