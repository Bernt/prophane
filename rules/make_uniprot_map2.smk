def input_make_uniprot_map(wildcards):
    dbinfo = get_db_info(wildcards.fname + ".yaml")
    xml = wildcards.fname + ".xml.gz"
    taxdump_map = dbinfo['taxdump_map']
    return [xml, taxdump_map]

rule make_uniprot_map:
  input:
    input_make_uniprot_map
  output:
    "{fname}.map"
  wildcard_constraints:
    fname = ".*/uniprot_[^/]+"
  message:
    "generating map {output[0]}"
  run:
    taxa = {}
    with open(input[1], "r") as handle:
        headline = handle.readline()[1:]
        for line in handle:
            taxid = line.split("\t")[0]
            taxa[taxid] = line

    acc_regex = re.compile('  <accession>([^<]+)</accession>')
    taxid_regex = re.compile('    <dbReference .*type="NCBI Taxonomy".*id="([0-9]+)".*/>')
    with gzip.open(input[0], 'rb') as gzhandle, open(output[0], 'w') as outhandle:
      outhandle.write("#accession\t" + headline)
      inhandle = io.BufferedReader(gzhandle)
      accs = set()
      taxids = set()
      organism=False
      for line in inhandle:
        line = line.decode()
        if line.startswith('<entry '):
          if len(accs) > 0:
            #writing map
            if len(taxids) > 1:
              sys.exit("error: multiple taxids given for " + ";".join(accs))
            elif len(taxids) == 0:
              sys.exit("error: no taxids given for " + ";".join(accs))
            for acc, taxid in itertools.product(accs, taxids):
              outhandle.write(acc + '\t' + taxa[taxid])
          accs = set()
          taxids = set()
          organism = False
        else:
          if line.startswith("  <organism"):
            organism = True
            continue
          if line.startswith("  </organism>"):
            organism = False
            continue
          match = acc_regex.match(line)
          if match:
            accs.add(str(match.group(1)))
            continue
          if organism:
              match = taxid_regex.match(line)
              if match:
                taxids.add(str(match.group(1)))
                continue
