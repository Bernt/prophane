def extract_from_fasta_header(accs, fasta_fnames):
    if len(accs) == 0:
        return {}
    accs_chars = set()
    for acc in accs:
        accs_chars.update([re.escape(x) for x in list(acc)])
    regex = re.compile("(?<=^>|[\x01|])([" + "".join(accs_chars) + "]+)(?=[| \x01]|$)")
    seqs = defaultdict(set)
    for fname in fasta_fnames:
        with open(fname, 'r') as handle:
            for record in SeqIO.parse(handle, "fasta"):
                for match in accs.intersection(set(regex.findall('>' + record.description))):
                    seqs[match].add(str(record.seq))
    return seqs

rule map_to_seq:
  input:
    pg_yaml = 'pgs/protein_groups.yaml',
    user_fastas = expand('{fname}', fname=config['input']['fastas']),
    seqdbs = config['seqdb']
  output:
    all='seqs/all.faa'
  message:
    "retrieving sequences ..."
  version: "0.11"
  run:
    # these files are not always produced and can therefore not be listed under output
    missing_seqs = os.path.join(os.getcwd(), 'seqs/missing_sequences.txt')
    ambig_seqs = os.path.join(os.getcwd(), 'seqs/ambiguous_sequences.txt')
    #file reset; remove files from potential prior executions
    for f in [missing_seqs, ambig_seqs]:
        if os.path.isfile(f):
            os.remove(f)

    #get accs
    accs = get_accs(input.pg_yaml)

    #screen fasta files submitted by user
    seqs = extract_from_fasta_header(accs, input.user_fastas)

    #accessions with multiple seqs assigned
    ambig = [x for x in seqs if len(seqs[x]) > 1]
    if ambig:
        with open(ambig_seqs, "w") as handle:
            for acc in ambig:
                handle.write(acc + "\n")
        sys.exit("error: ambiguous sequence information in submitted fasta files.\nsee " + ambig_seqs + " for more information")


    #missings seqs
    missing = [x for x in accs if not x in seqs]
    if missing:
        with open(missing_seqs, "w") as handle:
            handle.write("\n".join(missing) + "\n")
        trace("error:\nmissing sequence information (" + str(len(missing)) + " accessions)\nsee " + missing_seqs + " for more information", True)


    #write fasta
    with open(output['all'], "w") as handle:
        for acc, seq in seqs.items():
            handle.write(">" + acc + "\n" + seq.pop() + "\n")
