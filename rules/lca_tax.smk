rule lca_tax:
  input:
    pg_yaml = 'pgs/protein_groups.yaml',
    acc_taxmap = 'tax/taxmap.txt',
    taxblast = get_taxblast_result(config)
  output:
    config['output']['tax_lca']
  message:
    "resolving taxonomic lineages ..."
  run:
    #get accs
    accs = get_accs(input.pg_yaml)

    #get taxa
    taxa = read_taxmap(input.acc_taxmap, config['taxlevel'])

    #tax
    if input.taxblast:
        with open(input.taxblast, "r") as inhandle:
            hits = []
            for line in inhandle:
                if len(line.strip()) == 0:
                    continue
                fields = line.split("\t")
                acc =  fields[0]
                if acc not in taxa:
                    taxa[acc] = []
                taxa[acc].extend(fields[7].split(";"))

    #analyse pgs
    pgs = load_yaml(input.pg_yaml)
    sep = pgs['style']['protein_sep']
    lca = []
    for pg in pgs['protein_groups']:
        tax = list(zip(*[taxa[x] for x in pg['proteins'].split(sep)]))
        heterog = False
        for t in range(len(tax)):
            if heterog:
                tax[t] = config['dictionary']['heterogenous']
            elif len(set(tax[t])) == 1:
                tax[t] = tax[t][0]
            else:
                tax[t] = config['dictionary']['heterogenous']
                heterog = True
        lca.append(tax)

    #write lca file
    with open(output[0], "w") as handle:
        handle.write("\n".join([str(i) + "\t" + "\t".join(lca[i]) for i in range(len(lca))]))
