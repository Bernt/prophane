def input_make_dmnd_db(wildcards):
    db = wildcards.dmnddb + ".gz"
    dbinfo = get_db_info(wildcards.dmnddb)
    acc2tax = dbinfo['acc2tax'][0]
    taxdump = dbinfo['taxdump']
    return [db, acc2tax, taxdump]

rule make_dmnd_db:
  input:
    input_make_dmnd_db
  output:
    "{dmnddb}.dmnd"
  message:
    "preparing diamond db for first use: " + os.path.basename("{output[0]}")
  log:
    "{dmnddb}.dmnd.log"
  version: "0.12"
  conda:
    "../envs/prophane.yaml"
  threads: 10
  shell:
    '''
    # temporary dir is placed in DB folder as its size can exceed multiple GBs
    F={output[0]}
    TMP_DIR=$(dirname "${{F}}")/TMP
    mkdir $TMP_DIR
    # diamond makedb -t /media/stephan/Elements/prophane --in {input[0]} --db {output[0]} --taxonnodes {input[2]} --taxonmap {input[1]} --threads {threads} > {log} 2>&1
    diamond makedb -t $TMP_DIR --in {input[0]} --db {output[0]} --threads {threads} > {log} 2>&1
    rm -r $TMP_DIR
    '''
