def linked_db_files(wildcards):
    dbinfo = get_db_info(wildcards.fname + ".yaml")
    return [wildcards.fname + ".gz", dbinfo['taxdump_map']] + dbinfo['acc2tax']

rule make_nr_map:
  input:
    linked_db_files
  output:
    "{fname}.map",
    "{fname}.unmapped.log"
  wildcard_constraints:
    fname = ".*/nr[^/]*"
  message:
    "generating taxmap {output[0]}"
  run:
    dbinfo = get_db_info(wildcards.fname + ".yaml")

    #get taxa
    taxa = {}
    with open(input[1], "r") as handle:
        headline = handle.readline()[1:]
        for line in handle:
            line = line.strip()
            taxid = line.split("\t")[0]
            taxa[taxid] = line

    #get accs
    #acc_regexp = re.compile(dbinfo['acc_regexp'])
    #accs = set()
    #with open(input[0], 'r') as handle:
    #    for line in handle:#
    #        if line.startswith(">"):
    #            accs.update([x.group(1) for x in acc_regexp.finditer(line)])

    #find acc2taxid links and write out
    tags_regexp = re.compile('<[^>]*>')
    with open(output[0], 'w') as outhandle, open(output[1], 'w') as outhandle2:
        outhandle.write("#accession\t" + headline)
        outhandle2.write("#accession\ttaxid")
        for fname in input[2:]:
            with gzip.open(fname, 'rb') as gzhandle:
              inhandle = io.BufferedReader(gzhandle)
              inhandle.readline() #skip head line
              for line in inhandle:
                fields  = line.decode().strip().split("\t")
                acc = fields[1]
                taxid = fields[2]
                #if acc in accs:
                if taxid in taxa:
                    outhandle.write(acc + '\t' + tags_regexp.sub('', taxa[taxid]).strip() + "\n")
                else:
                    outhandle2.write(acc + '\t' + taxid + "\n")
