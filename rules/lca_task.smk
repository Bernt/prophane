def get_lca(data, heterog_label):
    data = list(zip(*data))
    heterog = False
    for t in range(len(data)):
        if heterog:
            data[t] = heterog_label
        elif len(set(data[t])) == 1:
            data[t] = data[t][0]
        else:
            data[t] = heterog_label
            heterog = True
    return "\t".join(data)

def input_return_lca_task(wildcards):
    n = int(wildcards.n)
    task = config['tasks'][n]
    pg_yaml = 'pgs/protein_groups.yaml'
    result_map = 'tasks/task.{n}.map'.format(n=wildcards.n)
    return [pg_yaml, result_map]

rule lca_task:
  input:
    input_return_lca_task
  output:
    'tasks/task.{n}.lca'
  message:
    "resolving taxonomic lineages ..."
  run:
    #process task map
    task = config['tasks'][int(wildcards.n)]
    dbinfo = get_db_info(get_db_yaml(task))
    lca_level = dbinfo['lca_level']
    hit_classes = {}
    headline = "\t".join(get_headline(input[1]).split("\t")[-lca_level:])
    with open(input[1], "r") as handle:
        for line in handle:
            line = line.strip()
            if len(line) == 0 and line[0] == "#":
                continue
            fields = line.split("\t")
            if fields[0] not in hit_classes:
                hit_classes[fields[0]] = []
            hit_classes[fields[0]].append(fields[-lca_level:])

    #analyse pgs
    pgs = load_yaml(input[0])
    sep = pgs['style']['protein_sep']
    with open(output[0], "w") as handle:
        handle.write("#pg\t" + headline + "\n")
        i = -1
        for pg in pgs['protein_groups']:
            i += 1
            group_classes = []
            for acc in pg['proteins'].split(sep):
                if acc not in hit_classes:
                    group_classes.append([config['dictionary']['unclassified']]*lca_level)
                else:
                    group_classes.extend(hit_classes[acc])
            handle.write(str(i) + "\t" + get_lca(group_classes, config['dictionary']['heterogenous']) + "\n")
