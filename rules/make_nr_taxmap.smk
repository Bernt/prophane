rule make_nr_taxmap:
  input:
    "{taxmap}.tar.gz"
  output:
    "{taxmap}.map"
  message:
    "generating taxmap {output[0]}"
  wildcard_constraints:
    taxmap = ".*/taxdump[^/]*"
  version:
    "0.1"
  run:
    taxlevel = set(config['taxlevel'])
    taxlevel_count = len(taxlevel)
    ranks = {}
    parents = {}
    names = {}
    merged = defaultdict(set)
    tar = tarfile.open(input[0], "r:gz")
    nodes_handle = tar.extractfile("nodes.dmp")
    names_handle = tar.extractfile("names.dmp")
    merged_handle = tar.extractfile("merged.dmp")
    nodes_handle = tar.extractfile("nodes.dmp")

    for line in nodes_handle:
        fields = [x.strip() for x in line.decode(tarfile.ENCODING).split('|')]
        ranks[fields[0]] = fields[2]
        parents[fields[0]] = fields[1]

    for line in merged_handle:
        fields = [x.strip() for x in line.decode(tarfile.ENCODING).split('|')]
        merged[fields[1]].add(fields[0])

    for line in names_handle:
        fields = [x.strip() for x in line.decode(tarfile.ENCODING).split('|')]
        if fields[3] == "scientific name":
            if fields[2] != "":
                names[fields[0]] = fields[2]
            else:
                names[fields[0]] = fields[1]

    tags_regexp = re.compile('<[^>]*>')
    with open(output[0], 'w') as handle:
        handle.write("#taxid\ttaxids_lineage\t" + "\t".join(config['taxlevel']) + "\n")
        for taxid in set(list(parents.keys()) + list(parents.values())):
            taxids = ['-'] * taxlevel_count
            lineage = ['-'] * taxlevel_count
            orig_taxid = taxid
            while True:
                if ranks[taxid] in taxlevel:
                    p = config['taxlevel'].index(ranks[taxid])
                    taxids[p] = taxid
                    lineage[p] = tags_regexp.sub('', names[taxid]).strip()
                if taxid == parents[taxid]:
                    break
                taxid = parents[taxid]
            taxids = ";".join(taxids)
            lineage = "\t".join(lineage)
            for taxid in [orig_taxid] + list(merged[orig_taxid]):
                handle.write(taxid + '\t' + taxids + '\t' + lineage + '\n')
