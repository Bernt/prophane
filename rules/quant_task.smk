def input_return_quant_task(wildcards):
    n = int(wildcards.n)
    task = config['tasks'][n]
    pg_yaml = 'pgs/protein_groups.yaml'
    quant = 'tasks/quant.tsv'
    lca = 'tasks/task.{n}.lca'.format(n=wildcards.n)
    return [pg_yaml, quant, lca]

rule quant_task:
  input:
    input_return_quant_task
  output:
    'tasks/task.{n}.quant'
  message:
    "add quantification to task ..."
  version:
    "0.1"
  params:
    quant_methods = {"min_nsaf", "max_nsaf", "mean_nsaf", "raw"}
  run:
    #read pgs
    pgs = load_yaml(input[0])
    samples = pgs['samples']

    #read quant
    quants = {}
    with open(input[1], "r") as handle:
        for line in handle:
            line = line.strip()
            if len(line) == 0 or line[0] == "#":
                continue
            fields = line.split("\t")
            pg = fields[0]
            if pg not in quants:
                quants[pg] = {}
            quants[pg][fields[1]] = fields[-2]

    #read lca
    lcas = {}
    with open(input[2], "r") as handle:
        headline = handle.readline().strip().split("\t")
        headline = "\t".join(headline[1:])
        for line in handle:
            line = line.strip()
            if len(line) == 0:
                continue
            fields = line.split("\t")
            pg = fields[0]
            lcas[fields[0]] = fields[1:]

    #calc
    lca_quant = {}
    for pg, lca in lcas.items():
        for sample in samples:
            lca_str = "\t".join(lca)
            if lca_str not in lca_quant:
                lca_quant[lca_str] = {}
            if sample not in lca_quant[lca_str]:
                lca_quant[lca_str][sample] = 0
            lca_quant[lca_str][sample] += float(quants[pg][sample])

    lca_order = sorted(lca_quant.keys())

    if 'sample_groups' in config:
        sg_indices = {}
        groups = sorted(config['sample_groups'].keys())
        for sg, samplelist in config['sample_groups'].items():
            sg_indices[sg] = [samples.index(x) for x in set(samplelist)]

    with open(output[0], "w") as handle:
        handle.write("sample\t" + headline + "\tquant\tsd\n")
        if 'sample_groups' in config:
            for g in groups:
                for lca in lca_order:
                    g_quant = [lca_quant[lca][x] for x in config['sample_groups'][g]]
                    handle.write(g + " (mean)\t" + lca + "\t" + str(numpy.mean(g_quant)) + "\t" + str(numpy.std(g_quant)) + "\n")
        for sample in samples:
            for lca in lca_order:
                handle.write(sample + "\t" + lca + "\t" + str(lca_quant[lca][sample]) + "\t-\n")
