rule gunzip:
  input:
    "{fname}"
  output:
    "{fname}"[:-3]
  wildcard_constraints:
    fname = ".*(?<!\.gz|\.tar\.)\.gz$"
  message:
      "unpacking file(s) ..."
  version: "0.11"
  shell:
    '''
    gunzip -kf {input[0]} {output[0]}
    '''
