def input_summarize(wildcards):
    files = ['pgs/protein_groups.yaml']
    for i in range(config['tasks']):
        files.append(get_task_lca(i))
    for i in range(config['tasks']):
        files.append(config['pg_mafft_report'].replace("{n}", str(i)))
    return files

rule summarize:
  input:
    input_summarize
  output:
    'summary.txt'
  message:
    "resolving taxonomic lineages ..."
  run:
      #load tax lcas
      tax_lcas = []
      with open(input[1], "r") as handle:
          for line in handle:
              line = line.strip()
              if len(line) == 0:
                  continue
              tax_lcas.append(line.split('\t')[1:])

      #load task lcas
      task_names = []
      task_lcas_files = []
      task_lcas = []
      task_fields = []
      n = -1
      for task in config['tasks']:
          n += 1
          if n == config['taxblast']:
              continue
          task_names.append(task['nickname'])
          task_lcas_files.append('tasks/task.{n}.lca'.format(n=n))
          task_lcas.append([])
          with open(task_lcas_files[-1], "r") as handle:
              task_fields.extend([task['nickname'] + "_LCA_" + x for x in handle.readline().strip().split("\t")[1:]])
              for line in handle:
                  task_lcas[-1].append(line.strip().split("\t")[1:])

      pgs = load_yaml(input[0])
      sep = pgs['style']['protein_sep']
      headline = ['#pg', "member_count", "members"] + [x + "_LCA" for x in config['taxlevel'] + task_names] + task_fields

      i = -1
      data = []
      with open(output[0], 'w') as handle:
        i = -1
        for pg in pgs['protein_groups']:
            i += 1
            data.append([])
            data[-1].append("pg_" + str(i))
            proteins = pg['proteins'].split(sep)
            data[-1].append(len(proteins))
            data[-1].extend(tax_lcas[i])
            for t in range(len(task_names)):
                data[-1].extend(task_lcas[t][i])

      for d in data + [headline]:
          print(d)

      exit()
