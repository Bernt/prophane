def input_map_task_result(wildcards):
    task = config['tasks'][int(wildcards.n)]
    dbinfo = get_db_info(get_db_yaml(task))
    best_hits_file = get_task_best_hits(wildcards.n)
    dbmap = get_db_map(get_db_yaml(task))
    files = [best_hits_file, dbmap]
    if dbinfo['scope'] == "tax":
        files.append('tax/taxmap.txt')
    return files

rule map_task_result:
  input:
    input_map_task_result
  output:
    'tasks/task.{n}.map'
  message:
    "generating task {wildcards.n} map ..."
  version:
    "0.1"
  run:
    #get task and db infos
    task = config['tasks'][int(wildcards.n)]
    dbinfo = get_db_info(get_db_yaml(task))

    #get hits
    hits = defaultdict(set)
    for line in iter_file(input[0]):
        fields = line.strip().split("\t")
        for hit in fields[1].split(";"):
            if dbinfo['type'] == "ncbi_nr":
                if hit.count("|") > 0:
                    continue
            elif dbinfo['type'].startswith("uniprot_"):
                hit = hit.split("|")[-1]
            hits[hit].add(fields[0])

    #get mapdata and write map
    with open(output[0], "w") as handle:
        handle.write("#acc\tsource\thit\t" + "\t".join(get_headline(input[1]).split("\t")[1:]) + "\n")
        if dbinfo['scope'] == "tax":
            for line in iter_file(input[-1]):
                handle.write(line + "\n")
        for line in iter_file(input[1]):
            fields = line.split("\t")
            if fields[0] in hits:
                data = [config['dictionary']['unclassified'] if x == "-" else x for x in fields[1:]]
                for query in hits[fields[0]]:
                    handle.write(query + "\t" + task['shortname'] + " [task " + wildcards.n + "]\t" + "\t".join(data) + "\n")
