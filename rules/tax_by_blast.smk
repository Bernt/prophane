def tax_by_blast_input():
    taxblast_task = config['tasks'][config['taxblast']]
    taxblast_result = config['output']['task_result'].replace("{n}", str(config['taxblast']))
    taxblast_query = taxblast_task['query']
    taxblast_taxmap = os.path.splitext(taxblast_task['db'])[0] + ".taxmap"
    db_info = db_info_file(taxblast_task['db'])
    return taxblast_query, taxblast_result, taxblast_taxmap, 'tax/missing_taxa_map.txt', db_info

rule tax_by_blast:
  input:
    tax_by_blast_input(),
  output:
    config['output']['taxblast_result'],
    config['output']['missing_taxa_blast']
  message:
    "mapping taxa to accessions ..."
  run:
    if os.path.getsize(input[3]) == 0:
        for o in output:
            open(o, "w").close()
    elif 'taxblast' not in config:
        os.rename(output[1], output[1] + ".log")
        sys.exit("error: missing taxonomic information (" + len(missing_taxa_accs) + ")")

    blasted_accs = set()
    print(input[3])
    with open(input[3], "r") as handle:
        for line in handle:
            line = line.strip()
            if len(line) == 0:
                continue
            blasted_accs.add(line)

    dbinfo = load_yaml(input[4])
    pattern = re.compile(";" + dbinfo['acc_regexp'])
    print(dbinfo['acc_regexp'])
    taxa = {}
    with open(input[1], "r") as handle:
        for line in handle:
            line = line.strip()
            if len(line) == 0:
                continue
            fields = line.split("\t")
            acc = fields[0]
            hits = fields[2].split(";")
            if acc not in taxa:
                taxa[acc] = set()
            taxa[acc].update([str(match.group(1)) for match in pattern.finditer(";" + fields[2])])

    missing_taxa_accs = blasted_accs - set(taxa.keys())

    with open(output[1], "w") as handle:
        handle.write("\n".join(missing_taxa_accs))

    if len(missing_taxa_accs) > 0:
        os.rename(output[1], output[1] + ".log")
        sys.exit("error: missing taxonomic information (" + str(len(missing_taxa_accs)) + " accessions)")

    all_hits = set([y for x in taxa.values() for y in x])
    with open(input[2], "r") as inhandle, open(output[1], "w") as outhandle:
        for line in inhandle:
            if line.strip() == "":
                continue
            fields = line.split("\t")
            if fields[0] in all_hits:
                outhandle.write("\n".join([x + "\t" + line for x in taxa if fields[0] in taxa[x]]))
