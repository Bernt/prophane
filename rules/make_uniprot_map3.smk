def input_make_uniprot_map(wildcards):
    dbinfo = get_db_info(wildcards.fname + ".yaml")
    db = wildcards.fname + ".fasta"
    idmap = dbinfo['idmap']
    return [db, idmap]

rule make_uniprot_map:
  input:
    input_make_uniprot_map
  output:
    "{fname}.map"
  wildcard_constraints:
    fname = ".*/uniprot_tax.txt"
  message:
    "generating taxmap {output[0]}"
  run:
    dbinfo = get_db_info(input[0] + ".yaml")
    accs = set()
    acc_regexp = dbinfo['acc_regexp']
    l = len(dbinfo['tag'])
    with open(input[0]) as handle:
        for line in handle:
            if line[0] == ">":
                m = acc_regexp.search(line)
                if m:
                    acc = m.group(0)[l:]
                    accs.add(acc)
    found = set()
    with open(input[1], 'r') as inhandle, open(output[0], 'w') as outhandle:
      outhandle.write("#accession\t" + headline)
      for line in inhandle:
          acc = line.split("\t")[0]
          if acc in accs:
              outhandle.write(acc + '\t' + taxa[taxid])
              found.add(acc)

    not_found = [x for x in accs if x not in found]
    if not_found:
        sys.exit("error: missing taxonomic information for: " + ", ".join(not_found))
