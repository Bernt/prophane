def input_make_uniprot_taxmap(wildcards):
    path = os.path.dirname(wildcards.taxmap)
    taxmap = os.path.join(path, "uniprot_tax.txt") #download by this link https://www.uniprot.org/taxonomy/?sort=&desc=&compress=no&query=&fil=&force=no&preview=true&format=tab&columns=id
    idmap = os.path.join(path, "idmapping.dat.gz")
    return [taxmap, idmap]

rule make_uniprot_taxmap:
  input:
    input_make_uniprot_taxmap
  output:
    "{taxmap}.map",
    "{taxmap}.unmapped.log"
  message:
    "generating taxmap {output[0]}"
  wildcard_constraints:
    taxmap = ".*/uniprot_tax"
  version:
    "0.1"
  run:
    #get taxa
    taxlevel = set(config['taxlevel'])
    taxlevel_count = len(taxlevel)
    ranks = {}
    parents = {}
    names = {}
    with open(input[0], "r") as handle:
        handle.readline()
        for line in handle:
            fields = line.strip().split("\t") + [''] * 9
            taxid = fields[0]
            name = fields[2]
            rank = fields[7].lower()
            parent = fields[9]

            names[taxid] = name
            parents[taxid] = parent
            ranks[taxid] = rank

    headline = "taxid\ttaxids_lineage\t" + "\t".join(config['taxlevel'])

    taxinfo = {}
    for taxid in names:
        taxids = ['-'] * taxlevel_count
        lineage = ['-'] * taxlevel_count
        orig_taxid = taxid
        while taxid != "":
            if ranks[taxid] in taxlevel:
                p = config['taxlevel'].index(ranks[taxid])
                taxids[p] = taxid
                lineage[p] = names[taxid]
            taxid = parents[taxid]
        taxinfo[orig_taxid] = '\t' + ";".join(taxids) + "\t" + "\t".join(lineage)

    del(names)
    del(parents)
    del(ranks)

    #get acc-taxid-link and write map
    with gzip.open(input[1], 'rb') as gzhandle, open(output[0], 'w') as outhandle, open(output[1], 'w') as outhandle2:
      outhandle.write("#accession\t" + headline + "\n")
      inhandle = io.BufferedReader(gzhandle)
      for line in inhandle:
        fields = line.decode().strip().split("\t")
        if fields[0] == 'M5BHJ8':
            print(fields)
        if fields[1] == "NCBI_TaxID":
            if fields[2] not in taxinfo:
                outhandle2.write(fields[0] + '\t' + fields[2] + "\n")
            else:
                outhandle.write(fields[0] + '\t' + fields[2] + "\t" + taxinfo[fields[2]] + "\n")
