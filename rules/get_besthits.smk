def best_hit_hmmer3(fname, alg):
    with open(fname, "r") as handle:
        handle.readline()
        handle.readline()
        line = handle.readline().strip()
        starts = [0] + [match.start()+2 for match in re.finditer(re.escape("- -"), line)] + [len(line)]
        starts.sort()
        cols = [(starts[x], starts[x+1]) for x in range(len(starts)-1)]
        if alg == "hmmsearch":
            col_acc = cols[0]
            col_hit = cols[3]
        elif alg == "hmmscan":
            col_acc = cols[2]
            col_hit = cols[1]
        col_score = cols[5]
        col_bias = cols[6]

        best_hits = {}
        for line in handle:
            if len(line.strip()) == 0 or line[0] == "#":
                continue
            acc = line[col_acc[0]:col_acc[1]].strip()
            score = line[col_score[0]:col_score[1]].strip()
            bias = line[col_bias[0]:col_bias[1]].strip()
            hit = line[col_hit[0]:col_hit[1]].strip().split(".")[0]

            if col_acc not in best_hits:
                best_hits[acc] = (hit, score, bias)
            elif score > best_hit[acc][1]:
                best_hits[acc] = (hit, score, bias)
            elif score == best_hits[acc][1] and bias < best_hits[acc][2]:
                best_hits[acc] = (hit, score, bias)
            elif score == best_hits[acc][1] and bias == best_hits[acc][2]:
                trace("error: equal best hits not yet considered in best_hit_hmmer3")

        for acc in best_hits:
            best_hits[acc] = best_hits[acc][0]

        return best_hits

def best_hit_blastp(fname, min_full_ident, db):
    dbinfo = get_db_info(db)
    acc_pattern = re.compile("(?:^|;)" + dbinfo['acc_hit_regexp'])
    with open(fname, "r") as handle:
        best_hits = {}
        for line in handle:
            fields = line.strip().split("\t")
            if len(fields) == 0:
                continue
            qacc = fields[0]
            hits = fields[1]
            qlen = int(fields[2])
            bitscore = float(fields[4])
            algnlen = int(fields[5])
            nident = int(fields[6])
            full_ident = algnlen/qlen * nident
            if full_ident >= min_full_ident:
                if qacc not in best_hits:
                    best_hits[qacc] = (hits, bitscore, full_ident)
                elif bitscore > best_hits[qacc][1]:
                    best_hits[qacc] = (hits, bitscore, full_ident)
                elif bitscore == best_hits[qacc][1] and full_ident > best_hits[qacc][2]:
                    best_hits[qacc] = (hits, bitscore, full_ident)
                elif bitscore == best_hits[qacc][1] and full_ident == best_hits[qacc][2]:
                    best_hits[qacc] = (best_hits[qacc][0] + ";" + hits, bitscore, full_ident)

        for acc in best_hits:
            hits = best_hits[acc][0]
            best_hits[acc] = []
            for match in  acc_pattern.finditer(hits):
                best_hits[acc].append(match.group(1))

        return best_hits

def best_hit_emapper(fname):
    with open(fname, "r") as handle:
        best_hits = {}
        for line in handle:
            line = line.strip()
            if len(line) == 0 or line[0] == "#":
                continue
            fields = line.split("\t")
            best_hits[fields[0]] = fields[10].split("|")[0] + "." + fields[1]
    return best_hits

rule get_besthits:
  input:
    'tasks/task.{n}.result'
    # lambda wildcards: get_task_result(wildcards.n)
    # config['output']['task_result']
  output:
    'tasks/task.{n}.best_hits'
  message:
    "collecting best hits of task {wildcards.n} ..."
  version:
    "0.1"
  threads:
    1
  run:
    task = config['tasks'][int(wildcards.n)]
    query_file = input[0]
    db_file = get_db_yaml(task)

    #diamond blastp
    if task['prog'] == "diamond blastp":
        out = []
        if "query-cover" in task:
          qc = task["query-cover"]
        elif "query-cover" in task['params']:
          qc = task['params']['query-cover']
        else:
          qc = 0
        for acc, hits in best_hit_blastp(input[0], qc, db_file).items():
            for hit in hits:
                out.append(acc + '\t' + hit)
        with open(output[0], "w") as handle:
            handle.write("\n".join(out))

    #hmmer
    elif task['prog'] in ["hmmscan", "hmmsearch"]:
        with open(output[0], "w") as handle:
            handle.write("\n".join([x[0] + "\t" + x[1] for x in best_hit_hmmer3(input[0], task['prog']).items()]))

    #egnogg mapper
    elif task['prog'] == "emapper":
        with open(output[0], "w") as handle:
            handle.write("\n".join([x[0] + "\t" + x[1] for x in best_hit_emapper(input[0]).items()]))

    #unknown tools
    else:
        trace("error: task " + wildcards.n + ": '" + task['prog'] + "' not allowed.", True)
