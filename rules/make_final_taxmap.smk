rule make_final_taxmap:
  input:
    acc_taxmap="{fname}.taxmap.tmp",
    result=""
    taxdump_map="/media/stephan/FUCHS_1T/dbs/taxdump.taxmap"
  output:
    "{fname}.taxmap"
  message:
      "generating taxmap ..."
  run:
    taxa = {}
    with open(input.taxdump_map, "r") as handle:
        headline = handle.readline()[1:]
        for line in handle:
            taxid = line.split("\t")[0]
            taxa[taxid] = line

    with gzip.open(input.xml, 'rb') as gzhandle:
      inhandle = io.BufferedReader(gzhandle)
      out = []
      accs = set()
      taxids = set()
      organism=False
      for line in inhandle:
        line = line.decode()
        if line.startswith('<entry '):
          if len(accs) > 0:
              out.append((accs, taxids))
          accs = set()
          taxids = set()
          organism=False
        elif line.startswith('  <accession>'):
          accs.add(line[13:].split("<")[0])
        elif line.startswith("  <organism>"):
          organism = True
        elif line.startswith("  </organism>"):
          organism = False
        elif organism and line.startswith('    <dbReference ') and line.endswith(' type="NCBI Taxonomy"/>\n'):
          taxids.add(line.split('id="')[1].split('"')[0])

    with open(output[0], 'w') as handle:
        handle.write(headline)
        for accs, taxids in out:
            if len(taxids) > 1:
                sys.exit("error: multiple taxids given for " + ";".join(accs))
            elif len(taxids) == 0:
                sys.exit("error: no taxids given for " + ";".join(accs))
            taxid = taxids.pop()
            [handle.write(x + '\t' + taxa[taxid]) for x in accs]
