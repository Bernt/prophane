rule parse_scaffold_protein_report:
  input:
    report = config['input']['report'],
    style = config['input']['report_style']
  output:
    'pgs/protein_groups.yaml'
  message:
    "processing protein report ..."
  version: "0.1"
  run:
    #process style info
    style = load_yaml(input.style)

    #style checks
    mandatory_style_fields = ['proteins_sep', 'eof', 'field_sep', 'sample_cols', 'proteins_col', 'quant_col']
    missing_tags = [x for x in mandatory_style_fields if x not in style]
    if missing_tags:
        trace('input style error in ' + input.style + ":\nmissing information:\n" + ", ".join(missing_tags), True)
    if style['eof'] == style['proteins_sep']:
        trace('input style error in ' + input.style + ":\n:\n" + ", ".join(missing_tags), True)

    #storing file style info
    protein_sep = style['proteins_sep']
    eof_tag = style['eof']
    field_sep = style['field_sep']
    sample_col = style['sample_cols']
    proteins_col = style['proteins_col']
    quant_col = style['quant_col']

    #process report
    protein_groups = {}
    with open(input.report, 'r') as handle:
        data_row = False
        eof = False

        for line in handle:
            line = line.strip()

            ##blank line
            if len(line) == 0:
                continue

            ##end of file
            if line == eof_tag:
                eof = True
                break

			##data rows
            elif data_row:
                fields = line.split(field_sep)
                sample = "::".join([fields[x] for x in sample_col])
                proteins = protein_sep.join(sorted(fields[proteins_col].split(protein_sep)))
                quant = int(fields[quant_col])
                if proteins not in protein_groups:
                    protein_groups[proteins] = {}
                if sample in protein_groups[proteins]:
                    trace("input data error:\n" + input.report + " contains multiple data for " + proteins + " and sample " + samples, True)
                protein_groups[proteins][sample] = quant

		  	##headline
            elif line == style['headline']:
                data_row = True

    #error checks
    if not data_row:
        trace("input format error " + input[0] + ":\nno style-specific headline", True)
    if not eof:
        trace("input format error " + input[0] + ":\nno style-specific eof tag", True)
    if len(protein_groups) == 0:
        trace("input format error " + input[0] + ":\nno data rows", True)

    #output: protein_group.yaml
    samples = sorted(set([y for x in protein_groups for y in protein_groups[x]]))
    yaml_data = {}

    ##samples
    yaml_data['samples'] = samples

    ##style definitions
    yaml_data['style'] = {}
    yaml_data['style']['protein_sep'] = protein_sep
    yaml_data['style']['quant_type'] = style['quant_type']

    ##pgs

    yaml_data['protein_groups'] = []
    for proteins, data in protein_groups.items():
        yaml_data['protein_groups'].append({})
        yaml_data['protein_groups'][-1]['proteins'] = proteins
        yaml_data['protein_groups'][-1]['quant'] = []
        for sample in samples:
            if sample in data:
                yaml_data['protein_groups'][-1]['quant'].append(data[sample])
            else:
                yaml_data['protein_groups'][-1]['quant'].append(0)

    ##writing
    with open(output[0], "w") as handle:
        handle.write(yaml.dump(yaml_data, default_flow_style=False))
