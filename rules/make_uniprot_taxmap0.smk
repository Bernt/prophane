rule make_nr_taxmap:
  input:
    "{taxdump}.tar.gz"
  output:
    "{taxdump}.map"
  message:
    "generating map {output[0]}"
  wildcard_constraints:
    taxdump = ".*/taxdump[^/]*"
  version:
    "0.1"
  run:
    taxlevel = config['taxlevel']
    ranks = {}
    parents = {}
    names = {}
    merged = defaultdict(set)
    tar = tarfile.open(input[0], "r:gz")
    nodes_handle = tar.extractfile("nodes.dmp")
    names_handle = tar.extractfile("names.dmp")
    merged_handle = tar.extractfile("merged.dmp")
    nodes_handle = tar.extractfile("nodes.dmp")

    for line in nodes_handle:
        fields = [x.strip() for x in line.decode(tarfile.ENCODING).split('|')]
        ranks[fields[0]] = fields[2]
        parents[fields[0]] = fields[1]

    for line in merged_handle:
        fields = [x.strip() for x in line.decode(tarfile.ENCODING).split('|')]
        merged[fields[1]].add(fields[0])

    for line in names_handle:
        fields = [x.strip() for x in line.decode(tarfile.ENCODING).split('|')]
        if fields[3] == "scientific name":
            if fields[2] != "":
                names[fields[0]] = fields[2]
            else:
                names[fields[0]] = fields[1]

    with open(output[0], 'w') as handle:
        handle.write("#taxid\tmerged\ttaxids_lineage\t" + "\t".join(taxlevel) + "\n")
        for taxid in set(list(parents.keys()) + list(parents.values())):
            taxdata = OrderedDict()
            for l in taxlevel:
                taxdata[l] = (None, None)
            taxid_lineage = []
            otaxid = taxid
            while True:
                if ranks[taxid] in taxdata:
                 taxdata[ranks[taxid]] = (taxid, names[taxid])
                if taxid == parents[taxid]:
                    break
                taxid = parents[taxid]
            taxlin = ";".join([str(x[0]) for x in taxdata.values()])
            taxdata = "\t".join([str(x[1]).strip() for x in taxdata.values()])
            handle.write(otaxid + '\t\t' + taxlin + "\t" + taxdata + '\n')
            for taxid in merged[otaxid]:
                handle.write(taxid + '\t' + otaxid + '\t' + taxlin + "\t" + taxdata + '\n')
