# explicitly set shell used by snakemake to bash
from utils.config import amend_config_with_defaults
from utils.db_handling import DbAccessor

shell.executable("/bin/bash")

import yaml
import glob
import re
import gzip
import io
import shutil
import tarfile
import itertools
import numpy
import datetime
from Bio import SeqIO
from collections import defaultdict
import os
import sys
#from collections import OrderedDict
import snakemake.utils as smk_utils
import utils


# set minimum snakemake version
smk_utils.min_version("5.4")
PROPHANE_VERSION = "3.1.4"

def trace(msg, die=False):
    '''prints a TRACE message to stdout and optionally stderr which causes program termination'''
    for m in msg.split("\n"):
        print(m)
    if die:
        sys.exit(msg)

def load_yaml(filename):
    '''reads data stored in a yaml file to a dict which is returned'''
    with open(filename, "r") as handle:
        return yaml.safe_load(handle)

def get_timestamp():
    return datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

def db_info_file(db):
    '''returns filename of the yaml file associated to a given database'''
    if os.path.basename(db).find == -1:
        return db
    return os.path.splitext(db)[0] + ".yaml"

def get_db_info(db):
    '''returns dict of yaml file info associated to a given database'''
    return load_yaml(db_info_file(db))

def get_db_map(db):
    return os.path.splitext(db)[0] + ".map"

def get_headline(fname):
    with open(fname, "r") as handle:
            return handle.readline().lstrip("#").strip()

def iter_file(fname):
    with open(fname, "r") as handle:
        for line in handle:
            line = line.strip()
            if len(line) > 0 and line[0] != "#":
                yield line.strip("\r\n")

def get_accs(pg_yaml):
    ''' get set containing all accessions stored in the defined protein group yaml file'''
    pgs = load_yaml(pg_yaml)
    sep = pgs['style']['protein_sep']
    accs = set()
    for pg in pgs['protein_groups']:
        accs.update(pg['proteins'].split(sep))
    return accs

def read_taxmap(taxmap, taxlevel):
    '''standard taxmap reader, only taxlevel information is returned'''
    taxa = {}
    l = -len(taxlevel)+1
    with open(taxmap, "r") as handle:
        for line in handle:
            line = line.strip()
            if len(line) == 0:
                continue
            fields = line.split("\t")
            taxa[fields[0]] = fields[l:]
    return taxa

def get_taxblast_result(config):
    if not config['taxblast']:
        return []
    return config['output']['task_result'].replace("{n}", str(config['taxblast']))

# def get_task_result(taskid):
#     return os.path.join(config['output_dir'], 'tasks/task.{n}.result'.format(n=taskid))
#     # return config['output']['task_result'].replace("{n}", str(taskid))

def get_task_best_hits(taskid):
    return 'tasks/task.{n}.best_hits'.format(n=taskid)

def get_task_lca(taskid):
    return 'tasks/task.{n}.lca'.format(n=taskid)

def get_task_map(taskid):
    return 'tasks/task.{n}.map'.format(n=taskid)

def get_task_quant(taskid):
    return 'tasks/task.{n}.quant'.format(n=taskid)

def get_mafft_report(n):
    return 'algn/mafft.{n}.txt'.format(n=n)

def get_hmm_exts():
    return ["h3f", "h3i","h3m","h3p"]

def find_groups_to_align(pg_yaml):
    data = load_yaml(pg_yaml)
    sep = data['style']['protein_sep']
    pgs = data['protein_groups']
    return [x for x in range(len(pgs)) if sep in pgs[x]['proteins']]


def get_db_yaml(task_config_dict):
    db_args = {'db_type': task_config_dict['db_type']}
    if 'db_version' in task_config_dict:
        db_args['db_version'] = task_config_dict['db_version']
    yaml_file = DbAccessor(config['db_base_dir'])\
        .get_database_yaml(**db_args)
    return yaml_file


# tried to put this into 'onstart' as a global variable, it is not accessible from the create info rule though
# Error: name 'START_TIME' is not defined
# global vars should not be used anyway, as they are potentially messed up in a cluster environment
# Can potentially be solved by: https://snakemake.readthedocs.io/en/stable/project_info/faq.html#i-want-to-pass-variables-between-rules-is-that-possible
START_TIME = get_timestamp()

if not config:
 sys.exit("error: no config file submitted")

config = utils.config.validate_config(config, schema=os.path.join(workflow.basedir, "schemas", "config.schema.yaml"))
if 'use_test_general_config' in config:
    general_config_yaml = config['use_test_general_config']
else:
    general_config_yaml = os.path.join(workflow.basedir, 'general_config.yaml')
config['db_base_dir'] = load_yaml(general_config_yaml)['db_base_dir']

workdir: config['output_dir']
# import pdb; pdb.set_trace()
onstart:
    print("job:", config['general']['job_name'])

#set databases
#BLASTDBs = set([x['db'] for x in config['tasks'] if x['prog'] == "blastp"])
#DMNDDBs = set([x['db'] for x in config['tasks'] if x['prog'] == "diamond blastp"])
#HMMLIBs = set([x['db'] for x in config['tasks'] if x['prog'].startswith("hmm")])
LCATASKS = list(range(len(config['tasks'])))
#if type(config['taxblast']) == int :
#    LCATASKS.remove(config['taxblast'])


#create output folders if necessary
# for outdir in set([os.path.dirname(os.path.abspath(x.replace("{n}", ""))) for x in config['output'].values()]):
#     os.makedirs(outdir, exist_ok = True)

pg_yaml_path = os.path.join(config['output_dir'], 'pgs/protein_groups.yaml')
if not os.path.isfile(pg_yaml_path):
    os.makedirs(os.path.dirname(pg_yaml_path), exist_ok=True)
    if not 'decoy_regex' in config:
        config['decoy_regex'] = ''
    else:
        config['decoy_regex'] = '-d \'' + config['decoy_regex'].replace("'", "\\'") + '\' '
    if 'report' in config['input']:
        shell(os.path.dirname(workflow.snakefile) + "/scripts/parse_scaffold_protein_report.py " + config['decoy_regex'] + config['input']['report_style'] + " " + config['input']['report'] + " " + pg_yaml_path)
    elif 'pd_xml' in config['input']:
        shell(os.path.dirname(workflow.snakefile) + "/scripts/parse_proteome_discoverer_output.py " + config['decoy_regex'] + config['input']['report_style'] + " " + config['input']['pd_xml'] + " " + config['input']['pd_psms'] + " " + pg_yaml_path)
    elif 'mpa_report' in config['input']:
        shell(os.path.dirname(workflow.snakefile) + "/scripts/parse_mpa_metaprotein_report.py " + config['decoy_regex'] + config['input']['report_style'] + " " + config['input']['mpa_report'] + " " + pg_yaml_path)

rule all:
    input:
        'summary.txt',
        expand('plots/plot_task.{n}.html', n = LCATASKS),
        'job_info.txt'

        #setup databases
        #expand("{db}.blastp", db=BLASTDBs),
        #expand("{db}.dmnd", db=DMNDDBs),
        #expand("{db}.{ext}", ext=get_hmm_exts(), db=HMMLIBs),

        #register pgs
        #config['output']['pg_yaml'],
        #config['output']['quant'],

        #taxonomic data retrieval
        #config['output']['taxmap_result'],
        #config['output']['tax_lca'],

        #analyses
        #dynamic(config['output']['pg_mafft']),

        #tasks
        #expand(config['output']['task_quant'], n = LCATASKS),

    threads: 3

#db handling
#include: "rules/gunzip.smk"
include: "rules/make_dmnd_db.smk"
include: "rules/make_hmm_lib.smk"
include: "rules/make_nr_taxmap.smk"
include: "rules/make_uniprot_taxmap.smk"
include: "rules/make_nr_map.smk"
include: "rules/make_uniprot_map.smk"
include: "rules/make_tigrfam_map.smk"
include: "rules/make_pfam_map.smk"
include: "rules/make_eggnog_map.smk"

#input and data mapping handling
#include: "rules/parse_scaffold_protein_report.smk"
include: "rules/map_to_seq.smk"
include: "rules/create_pg_fasta.smk"
include: "rules/tax_by_taxmap.smk"

#analyses
include: "rules/run_mafft.smk"
include: "rules/report_mafft.smk"
include: "rules/run_task.smk"
include: "rules/get_besthits.smk"
include: "rules/map_task_result.smk"
include: "rules/lca_task.smk"
include: "rules/quant_pg.smk"
include: "rules/quant_task.smk"
include: "rules/create_summary.smk"

#plotting
# include: "rules/create_krona_xml.smk"
# include: "rules/create_krona_html.smk"
include: "rules/create_krona.smk"

#info
include: "rules/create_info.smk"
