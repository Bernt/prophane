import yaml


def load_yaml(filename):
    '''reads data stored in a yaml file to a dict which is returned'''
    with open(filename, "r") as handle:
        return yaml.safe_load(handle)
