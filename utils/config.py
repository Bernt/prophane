import os

from snakemake import utils as smk_utils


def amend_config_with_defaults(
        user_config_dict,
        default_config_dict
):
    # default_cfg = load_yaml(default_config_path)
    user_config_dict = smk_utils.update_config(default_config_dict, user_config_dict)
    return user_config_dict


def validate_config(config_dict, schema="schemas/config.schema.yaml"):
    """
    Validates  config_dict with against config schema.
    Sets default values if not set.
    :param config_dict: dictionary to be validated
    :param schema: yaml or json schema
    :return:
    """
    smk_utils.validate(config_dict, schema, set_default=True)
    return config_dict


def test_validation():
    from utils.input_output import load_yaml
    test_dict = {"general": {'job_name': 'moep', 'job_comment': 'tuut'}}
    test_dict = load_yaml('../tests/test01/job_dir/input/config.yaml')
    schema = "../schemas/config.schema.yaml"
    test_dict = validate_config(test_dict, schema)
    from pprint import pprint
    pprint(test_dict)
