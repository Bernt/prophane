#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from utils.input_output import load_yaml
import argparse
import glob
import os
import pandas as pd
from pkg_resources import parse_version


class Database(object):
    def __init__(self, yaml):
        # if not os.path.isabs(yaml):
        #     raise ValueError('absolute path required, got:\n\t{}'.format(yaml))
        self.yaml = yaml
        self._config = self._load_yaml()

    @staticmethod
    def is_database_yaml(yaml):
        required_keys = ['name', 'type', 'version', 'comment', 'scope']
        yaml_dict = load_yaml(yaml)
        if type(yaml_dict) is not dict:
            return False
        if all(key in yaml_dict for key in required_keys):
            return True
        else:
            return False

    def _load_yaml(self):
        if self.is_database_yaml(self.yaml):
            return load_yaml(self.yaml)
        else:
            raise ValueError("Not a valid database yaml: {}".format(self.yaml))

    def download_db(self):
        pass

    def get_version(self):
        return self._config['version'].__str__()

    def get_type(self):
        return self._config['type']

    def get_scope(self):
        return self._config['scope']

    def get_yaml(self):
        return self.yaml


class DbAccessor(object):
    def __init__(self, db_path):
        self._db_base_path = db_path
        self._db_yamls = self._get_db_yamls()
        self._df_database_infos = self._get_database_infos()

    def _get_db_yamls(self):
        db_yamls = [f for f in glob.glob(self._db_base_path + "/**/*.yaml", recursive=True) if Database.is_database_yaml(f)]
        if not db_yamls:
            raise ValueError('No valid databases were found in \n\t{}'.format(self._db_base_path))
        return db_yamls

    def _get_database_infos(self):
        df = pd.DataFrame()
        for f in self._db_yamls:
            s = pd.Series()
            db = Database(f)
            s['db_type'] = db.get_type()
            s['scope'] = db.get_scope()
            s['db_version'] = db.get_version()
            s['db_yaml'] = db.get_yaml()
            df = df.append(s, ignore_index=True)
        return df

    def _get_max_version(self, list_of_version_numbers):
        v_max = ''
        for v in list_of_version_numbers:
            if parse_version(v) >= parse_version(v_max):
                v_max = v
        return v_max

    def get_database_yaml(self, db_type, version=None):
        df = self._df_database_infos
        df = df[df['db_type'] == db_type]
        if version:
            yaml = df[df['db_version'] == version]['db_yaml'].values[0]
        else:
            ver_max = self._get_max_version(df['db_version'].values)
            yaml = df[df['db_version'] == ver_max]['db_yaml'].values[0]
        return yaml

    def print_summary(self):
        print('\n')
        print('Databases available in "{}":\n'.format(self._db_base_path))
        df = self._df_database_infos.sort_values(by=['scope', 'db_type'])
        df['db_yaml'] = df['db_yaml'].apply(lambda x: os.path.relpath(os.path.abspath(x), self._db_base_path))
        print(df[['scope', 'db_type', 'db_version', 'db_yaml']].to_string(index=False))
        pass

