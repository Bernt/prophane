#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
from utils.db_handling import DbAccessor
from utils.input_output import load_yaml
import sys


def main():
    prophane_path = os.path.dirname(__file__)
    general_config = os.path.join(prophane_path, 'general_config.yaml')
    db_base_path = load_yaml(general_config)['db_base_dir']
    try:
        dbaccessor = DbAccessor(db_base_path)
    except ValueError as e:
        print(str(e))
        sys.exit(1)
    dbaccessor.print_summary()
    pass


if __name__ == "__main__":
    main()
