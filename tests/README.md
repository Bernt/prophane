# README: PROPHANE TEST

This folder provides test databases for all databases except eggNOG and a test job to test all funcionalities except functional predictions based on eggNOG.

This folder might copied wherever you want. However, the content has not to be changed.
To initialize the test databases and test job please start the setup_test.py which needs the path to your prophane instance as only parameter.

The job can be started within the test_job directory itself.
