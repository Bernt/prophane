#!/usr/bin/env python3
# -*- coding: utf-8 -*-
__authors__ = ["Henning Schiebenhoefer", "Tobias Marschall", "Marcel Martin", "Johannes Köster"]
__copyright__ = "Copyright 2015, Johannes Köster"
__email__ = "koester@jimmy.harvard.edu"
__license__ = "MIT"

import os
import shutil
from os.path import join
import hashlib
import urllib
from shutil import which
import pytest
from snakemake import snakemake
import pandas as pd

if not which("snakemake"):
    raise Exception("snakemake not in PATH. For testing, install snakemake with "
                    "'pip install -e .'. You should do this in a separate environment "
                    "(via conda or virtualenv).")


PROPHANE_DIR = os.path.dirname(os.path.dirname(__file__))


def dpath(path):
    """
    get path to a data file (relative to the directory this
    test lives in)
    """
    return os.path.realpath(join(os.path.dirname(__file__), path))


def md5sum(filename):
    data = open(filename, 'rb').read()
    return hashlib.md5(data).hexdigest()


# test skipping
def is_connected():
    try:
        urllib.request.urlopen("http://www.google.com", timeout=1)
        return True
    except urllib.request.URLError:
        return False


def is_ci():
    return "CI" in os.environ


def has_gcloud_service_key():
    return "GCLOUD_SERVICE_KEY" in os.environ


def has_gcloud_cluster():
    return "GCLOUD_CLUSTER" in os.environ


gcloud = pytest.mark.skipif(not is_connected()
                            or not has_gcloud_service_key()
                            or not has_gcloud_cluster(),
                            reason="Skipping GCLOUD tests because not on "
                                   "CI, no inet connection or not logged "
                                   "in to gcloud.")

connected = pytest.mark.skipif(not is_connected(), reason="no internet connection")

ci = pytest.mark.skipif(not is_ci(), reason="not in CI")


def copy(src, dst):
    if os.path.isdir(src):
        shutil.copytree(src, os.path.join(dst, os.path.basename(src)))
    else:
        shutil.copy(src, dst)


@pytest.fixture()
def prophane_test_dbs(tmpdir):
    from tests import setup_test
    copy(os.path.join(os.path.dirname(__file__), 'test_dbs'), str(tmpdir))
    setup_test.set_db_config_paths(
        db_base_dir=str(tmpdir.join('test_dbs'))
    )
    # call([join(os.path.dirname(__file__), 'setup_test.py'), str(tmpdir)])
    return


# def test_db_preparation(tmpdir):
#     prophane_test_dbs(tmpdir)
#     src_files = set([x.split('test_dbs/')[1] for x in glob.glob("test_dbs/**/*")])
#     dest_files = set([x.split('test_dbs/')[1] for x in glob.glob(str(tmpdir) + '/test_dbs/**/*')])
#     assert src_files == dest_files


@pytest.fixture()
def prophane_input():
    def _setup(test_dir, tmpdir, db_dir=''):
        """
        Copy files from test_dir/job_dir to tmpdir/job_dir
        and adjust preliminary paths of config file copied from test_dir/job_dir/
        :param test_dir: folder containing 'job_dir'
        :param tmpdir: 'test_dir/job_dir' is copied here
        :param db_dir: database base directory; needed for updating database paths in input config; if not specified,
            'tmpddir/test_dbs' is set as database base path
        :return:
            path of config file
            job directory
        """
        db_dir = db_dir if db_dir is not '' else os.path.join(tmpdir, 'test_dbs')
        assert os.path.isdir(db_dir)
        from tests import setup_test
        job_dir = os.path.join(tmpdir, 'job_dir')
        copy(os.path.join(os.path.dirname(__file__), test_dir, "job_dir"), tmpdir)
        yamls = setup_test.set_prophane_config_paths(
            db_base_dir=db_dir,
            prophane_base_dir=PROPHANE_DIR,
            job_base_dir=job_dir,
            general_config_path=os.path.join(job_dir, "general_config.yaml")
        )
        job_yamls = [yaml for yaml in yamls if 'input/' in os.path.relpath(yaml, job_dir)]
        assert len(job_yamls) == 1, 'got "{}" prophane job config yamls. Expected exactly 1.'\
            .format(len(yamls))
        return job_yamls[0], job_dir
    return _setup

# original snakemake test runner
# def run(path,
#         shouldfail=False,
#         snakefile="Snakefile",
#         subpath=None,
#         no_tmpdir=False,
#         check_md5=True, cores=3, **params):
#     """
#     Test the Snakefile in path.
#     There must be a Snakefile in the path and a subdirectory named
#     expected-results.
#     """
#     results_dir = join(path, 'expected-results')
#     snakefile = join(path, snakefile)
#     assert os.path.exists(snakefile)
#     assert os.path.exists(results_dir) and os.path.isdir(
#         results_dir), '{} does not exist'.format(results_dir)
#     with tempfile.TemporaryDirectory(prefix="snakemake-") as tmpdir:
#         config = {}
#         # handle subworkflow
#         if subpath is not None:
#             # set up a working directory for the subworkflow and pass it in `config`
#             # for now, only one subworkflow is supported
#             assert os.path.exists(subpath) and os.path.isdir(
#                 subpath), '{} does not exist'.format(subpath)
#             subworkdir = os.path.join(tmpdir, "subworkdir")
#             os.mkdir(subworkdir)
#             # copy files
#             for f in os.listdir(subpath):
#                 copy(os.path.join(subpath, f), subworkdir)
#             config['subworkdir'] = subworkdir
#
#         # copy files
#         for f in os.listdir(path):
#             print(f)
#             copy(os.path.join(path, f), tmpdir)
#
#         # run snakemake
#         success = snakemake(snakefile,
#                             cores=cores,
#                             workdir=path if no_tmpdir else tmpdir,
#                             stats="stats.txt",
#                             config=config, **params)
#         if shouldfail:
#             assert not success, "expected error on execution"
#         else:
#             assert success, "expected successful execution"
#             for resultfile in os.listdir(results_dir):
#                 if resultfile == ".gitignore" or not os.path.isfile(
#                         os.path.join(results_dir, resultfile)):
#                         # this means tests cannot use directories as output files
#                         continue
#                 targetfile = join(tmpdir, resultfile)
#                 expectedfile = join(results_dir, resultfile)
#                 assert os.path.exists(
#                     targetfile), 'expected file "{}" not produced'.format(
#                         resultfile)
#                 if check_md5:
#                     # if md5sum(targetfile) != md5sum(expectedfile):
#                     #     import pdb; pdb.set_trace()
#                     assert md5sum(targetfile) == md5sum(
#                         expectedfile), 'wrong result produced for file "{}"'.format(
#                             resultfile)


def run_prophane(
        path,
        tmpdir,
        shouldfail=False,
        no_tmpdir=False,
        check_md5=True, cores=3, **params
):
    """
    Test the Prophane on 'input' folder in path.
    There must be a subdirectory named
    expected-results in the path.
    """
    expected_results_dir = join(os.path.dirname(__file__), path, 'expected-results')
    snakefile = join(PROPHANE_DIR, 'Snakefile')
    assert os.path.exists(snakefile)
    assert os.path.exists(expected_results_dir) and os.path.isdir(
        expected_results_dir), '{} does not exist'.format(expected_results_dir)

    # run snakemake
    success = snakemake(snakefile,
                        cores=cores, use_conda=True,
                        workdir=path if no_tmpdir else tmpdir,
                        stats="stats.txt",
                        **params)
    if shouldfail:
        assert not success, "expected error on execution"
    else:
        assert success, "expected successful execution"
        compare_with_expected_results(tmpdir, expected_results_dir, check_md5)


def assert_dataframes_equal(df1, df2):
    pd.testing.assert_frame_equal(df1, df2, check_dtype=False)


def assert_summary_content_equal(file1, file2):
    def read_summary(f):
        return pd.read_csv(f, sep="\t")
    dfs_ready_for_comparison = []
    for file in [file1, file2]:
        df = read_summary(file)
        # drop protein group number column, as this is not reproducible at the moment
        df = df.drop(columns="#pg")
        # drop member rows
        df = df[df['level'] == 'group']
        # sort by protein group member list
        df = df.sort_values('members_identifier')
        # reindex rows
        df = df.reset_index(drop=True)
        dfs_ready_for_comparison.append(df)
    try:
        assert_dataframes_equal(*dfs_ready_for_comparison)
    except AssertionError as e:
        raise AssertionError(
            f"""
summary.txt files are not equal!
left file:\t{file1}
right file:\t{file2}

File comparison result (first unequal column):
{e}
            """
        )


def test_assert_summary_content_equal():
    # equal summaries of different order
    f1, f2 = [join(os.path.dirname(__file__), "test_is_summary_content_equal/equal", f)
              for f in ["summary1.txt", "summary2.txt"]]
    assert_summary_content_equal(f1, f2)    # , 'summary files [{}, {}] are equal'.format(f1, f2)


def test_assert_summary_content_not_equal():
    # unequal summaries
    f1, f2 = [join(os.path.dirname(__file__), "test_is_summary_content_equal/unequal", f)
              for f in ["summary1.txt", "summary2.txt"]]
    with pytest.raises(AssertionError):
        assert_summary_content_equal(f1, f2)    # , 'summary files [{}, {}] are equal'.format(f1, f2)


def compare_with_expected_results(result_dir, expected_results_dir, check_md5=True):
    for current_loop_dir, subdirs, list_expected_files in os.walk(expected_results_dir):
        for expected_filename in list_expected_files:
            if expected_filename == ".gitignore":
                continue
            expected_filepath = join(current_loop_dir, expected_filename)
            rel_file_path = os.path.relpath(expected_filepath, expected_results_dir)
            produced_filepath = os.path.abspath(join(result_dir, rel_file_path))
            assert os.path.exists(
                produced_filepath), 'expected file "{}" not produced'.format(
                rel_file_path)
            if expected_filename == "summary.txt":
                assert_summary_content_equal(expected_filepath, produced_filepath)
            elif check_md5:
                assert md5sum(produced_filepath) == md5sum(expected_filepath), \
                    'wrong result produced for file "{}"'.format(rel_file_path)


def test_full_analysis_and_file_presence(prophane_test_dbs, prophane_input, tmpdir):
    test_ressource_dir = 'test_full_analysis_and_file_presence'
    config_file, job_dir = prophane_input(test_ressource_dir, str(tmpdir))
    run_prophane(test_ressource_dir, str(job_dir), configfile=config_file, check_md5=False)
