import utils.db_handling as db_hand
import pytest


# @pytest.fixture()
def database_object(yaml="../test_dbs/nr/20180808/nr.yaml"):
    db = db_hand.Database(yaml)
    return db


@pytest.fixture()
def db_accessor(db_base_path="../test_dbs/"):
    return db_hand.DbAccessor(db_base_path)


testdata = [
    ('../test_dbs/nr/20180808/nr.yaml', '2018-08-08', 'ncbi_nr', 'tax'),
    ('../test_dbs/pfams/31/Pfam-A.yaml', '31', 'pfams', 'func'),
    ('../test_dbs/swissprot/20180808/uniprot_sprot.yaml', '2018-08-08', 'uniprot_sp', 'tax'),
    ('../test_dbs/tigrfams/15/TIGRFAMs_15.0_HMM.yaml', '15.0', 'tigrfams', 'func'),
    ('../test_dbs/trembl/20180808/uniprot_trembl.yaml', '2018-08-08', 'uniprot_tr', 'tax'),
    ('../test_dbs/uniprot_complete/20180808/uniprot_complete.yaml', '2018-08-08', 'uniprot_complete', 'tax')
]


@pytest.mark.parametrize("db,ver,db_type,scope", testdata)
def test_return_of_db_version(db, ver, db_type, scope):
    db = database_object(db)
    assert db.get_version() == ver


@pytest.mark.parametrize("db,ver,db_type,scope", testdata)
def test_return_of_db_type(db, ver, db_type, scope):
    db = database_object(db)
    assert db.get_type() == db_type


@pytest.mark.parametrize("db,ver,db_type,scope", testdata)
def test_return_of_scope(db, ver, db_type, scope):
    db = database_object(db)
    assert db.get_scope() == scope


def test_fail_of_wrong_yaml_type():
    with pytest.raises(ValueError):
        database_object("../test_dbs/skip/skip.yaml")


def test_db_accessor(db_accessor):
    assert db_accessor._get_db_yamls() == [
        '../test_dbs/nr/20180808/nr.yaml',
        '../test_dbs/pfams/31/Pfam-A.yaml',
        '../test_dbs/swissprot/20180808/uniprot_sprot.yaml',
        '../test_dbs/taxdump/20180808/taxdump.yaml',
        '../test_dbs/tigrfams/15/TIGRFAMs_15.0_HMM.yaml',
        '../test_dbs/trembl/20180808/uniprot_trembl.yaml',
        '../test_dbs/uniprot_complete/20180808/uniprot_complete.yaml'
    ]


def test_get_ncbi_yaml(db_accessor):
    assert db_accessor.get_database_yaml(db_type='ncbi_nr') == \
        '../test_dbs/nr/20180808/nr.yaml'
    s = db_accessor._df_database_infos.iloc[0, :]
    s['db_version'] = '2018-09-10'
    s['db_yaml'] = 'foodidoo'
    db_accessor._df_database_infos.append(
        s,
        ignore_index=True
    )
    assert db_accessor.get_database_yaml(db_type='ncbi_nr') == \
        'foodidoo'


def test_accessor_summary(db_accessor):
    db_accessor.print_summary()
