#!/bin/bash

INPUT_YAML=$(readlink -f $1)

INPUT_PATH=${INPUT_YAML%/*}
INPUT_FASTA=$(ls ${INPUT_PATH}/*.fasta)
INPUT_PROT_GROUPS=$(ls ${INPUT_PATH}/*.csv)
OUTPUT_PATH=${INPUT_PATH%/input}
# DB_PATH=
SAMPLE_NAME=Sample

OLD_INPUT_FASTA=/home/hschieb/data/2018_mps_challenge/prophane_ana_stephan/input/Contest_Sample_1_SIHMUI_Bd16_proteinGroups.fasta
OLD_INPUT_PROT_GROUPS=/home/hschieb/data/2018_mps_challenge/prophane_ana_stephan/input/Contest_Sample_1_SIHMUI_Bd16_ctrl.mgf_metaproteins_neu.csv
OLD_OUTPUT_PATH=/home/hschieb/data/2018_mps_challenge/prophane_ana_stephan
# OLD_DB_PATH=
OLD_SAMPLE_NAME=sample1

#echo \
sed -i "s;$OLD_INPUT_FASTA;$INPUT_FASTA;" $INPUT_YAML
#echo \
sed -i "s;$OLD_INPUT_PROT_GROUPS;$INPUT_PROT_GROUPS;" $INPUT_YAML
#echo \
sed -i "s;$OLD_OUTPUT_PATH;$OUTPUT_PATH;" $INPUT_YAML
#echo \
sed -i "s;- \"$OLD_SAMPLE_NAME\";- \"$SAMPLE_NAME\";" $INPUT_YAML
