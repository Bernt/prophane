#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
input: file names of scaffold protein report, style yaml and output
output: protein group yaml for prophane
"""

import argparse
import yaml
import re
import sys

def parse_scaffold_protein_report(report_fname, style_fname, out_fname, decoy_regex=False):
    print("processing protein report ...")
    style = load_yaml(style_fname)

    #style checks
    mandatory_style_fields = ['proteins_sep', 'eof', 'field_sep', 'sample_cols', 'proteins_col', 'quant_col']
    missing_tags = [x for x in mandatory_style_fields if x not in style]
    if missing_tags:
        sys.exit('input style error in ' + input.style + ":\nmissing information:\n" + ", ".join(missing_tags))
    if style['eof'] == style['proteins_sep']:
        sys.exit('input style error in ' + input.style + ":\n:\n" + ", ".join(missing_tags))

    #storing file style info
    protein_sep = style['proteins_sep']
    eof_tag = style['eof']
    field_sep = style['field_sep']
    sample_col = style['sample_cols']
    proteins_col = style['proteins_col']
    quant_col = style['quant_col']

    #decoy
    if decoy_regex:
        decoy_regex = re.compile(decoy_regex)

    #process report
    protein_groups = {}
    with open(report_fname, 'r') as handle:
        data_row = False
        eof = False

        for line in handle:
            line = line.strip()

            ##blank line
            if len(line) == 0:
                continue

            ##end of file
            if line == eof_tag:
                eof = True
                break

			##data rows
            elif data_row:
                fields = line.split(field_sep)
                sample = "::".join([fields[x] for x in sample_col])
                if decoy_regex:
                    proteins = protein_sep.join(sorted([x for x in fields[proteins_col].split(protein_sep) if not decoy_regex.search(x)]))
                else:
                    proteins = protein_sep.join(sorted([x for x in fields[proteins_col].split(protein_sep)]))
                if proteins == "":
                    continue
                quant = int(fields[quant_col])
                if proteins not in protein_groups:
                    protein_groups[proteins] = {}
                if sample in protein_groups[proteins]:
                    sys.exit("input data error:\n" + report_fname + " contains multiple data for " + proteins + " and sample " + sample)
                protein_groups[proteins][sample] = quant

		  	##headline
            elif line == style['headline']:
                data_row = True

    #error checks
    if not data_row:
        sys.exit("input format error in " + report_fname + ":\nno style-specific headline")
    if not eof:
        sys.exit("input format error in " + report_fname + ":\nno style-specific eof tag")
    if len(protein_groups) == 0:
        sys.exit("input format error in " + report_fname + ":\nno data rows")

    #output: protein_group.yaml
    samples = sorted(set([y for x in protein_groups for y in protein_groups[x]]))
    yaml_data = {}

    ##samples
    yaml_data['samples'] = samples

    ##style definitions
    yaml_data['style'] = {}
    yaml_data['style']['protein_sep'] = protein_sep
    yaml_data['style']['quant_type'] = style['quant_type']

    ##pgs

    yaml_data['protein_groups'] = []
    protein_set = set()
    for proteins, data in protein_groups.items():
        yaml_data['protein_groups'].append({})
        yaml_data['protein_groups'][-1]['proteins'] = proteins
        protein_set.update(proteins.split(protein_sep))
        yaml_data['protein_groups'][-1]['quant'] = []
        for sample in samples:
            if sample in data:
                yaml_data['protein_groups'][-1]['quant'].append(data[sample])
            else:
                yaml_data['protein_groups'][-1]['quant'].append(0)

    ##writing
    with open(out_fname, "w") as handle:
        handle.write(yaml.dump(yaml_data, default_flow_style=False))

    ##output
    print("protein groups:", len(protein_groups))
    print("unique proteins:", len(protein_set))

def load_yaml(filename):
    '''reads data stored in a yaml file to a dict which is returned'''
    with open(filename, "r") as handle:
        return yaml.safe_load(handle)

def parse_args():
    parser = argparse.ArgumentParser(
        description="creates a protein group yaml for prophane based on scaffold's protein report and report style"
    )
    parser.add_argument('style', type=argparse.FileType('r'),
                    help='protein report style file (yaml)')
    parser.add_argument('report', type=argparse.FileType('r'),
                        help='scaffold\'s protein report (txt)')
    parser.add_argument('out', type=argparse.FileType('w'),
                        help='prophane\'s config file (yaml)')
    parser.add_argument('-d', '--decoy', type=str, default=False,
                        help='regular expression matching to decoy accesions (will be excluded)')

    return parser.parse_args()


def main():
    args = parse_args()
    report_fname = args.report.name
    args.report.close()
    style_fname = args.style.name
    args.style.close()
    out_fname = args.out.name
    args.out.close()
    parse_scaffold_protein_report(report_fname, style_fname, out_fname, args.decoy)


if __name__ == "__main__":
    main()
