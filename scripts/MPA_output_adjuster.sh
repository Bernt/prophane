#!/bin/bash

MPA_OUTPUT=$1

FNAME=$(basename $MPA_OUTPUT)
SAMPLE_NAME=${FNAME%%.*}

#replace windows line endings
# sed -i 's/\r\n/\n/' $MPA_OUTPUT
sed -i 's/\r$//' $MPA_OUTPUT

# add column
sed -i "s/$/Sample\t/" $MPA_OUTPUT
# sed -i "s/$/\t$SAMPLE_NAME/" $MPA_OUTPUT
# sed -i "0,/$SAMPLE_NAME/{s/$SAMPLE_NAME/Sample/}" $MPA_OUTPUT
